MusicBass 100
{
	pan $ff
	rest 256	; Drum solo
	rest 256	; Drum solo
	loop -1
		loop 4
			call BassA	; 256
			call BassB	; 256
		endloop
	endloop
	end
}

;**************************************
; BassA - 256
;**************************************
BassA
{
	loop 2
		using Gargletar
		c.2 8
		rest 8
		c.2 8
		rest 56
		a.2 8
		rest 16
		as2 8
		rest 16
	endloop
	return
}

;**************************************
; BassB - 256
;**************************************
BassB
{
	using Gargletar
	c.2 8
	rest 8
	g.1 8
	rest 56
	a.1 8
	rest 16
	as1 8
	rest 16

	c.2 8
	rest 8
	rest 8
	rest 56
	rest 8
	rest 16
	rest 8
	rest 16
	return
}
