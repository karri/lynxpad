'''
    The sass instruments are grouped into 6
    categories

    8 percussion instruments
      Kick
      Bass_Drum
    8 string instruments
    8 solo instruments
    8 slides
    8 computer sounds
'''
import os
import json
from sndlib import sndlib

class sass:
    def __init__(self):
        self.sndlib = sndlib()
        self.names = self.sndlib.sndnames().copy()
        self.src = []
        self.desc = []
        for s in self.names:
            if s is None:
                self.src.append(None)
                self.desc.append(None)
            else:
                fname = s + '.json'
                if os.path.exists(fname):
                    with open(fname) as f:
                        rec = json.loads(f.read())
                        if not rec is None:
                            self.src.append(rec['src'])
                            self.desc.append(rec['desc'])
                        else:
                            self.src.append(None)
                            self.desc.append(None)
                else:
                    self.src.append(None)
                    self.desc.append(None)

    def clearused(self):
        self.used = []
        for i in range(0, len(self.names)):
            self.used.append(False)

    def setused(self, nr):
        self.used[nr] = True

    def saveall(self):
        for i in range(0, len(self.names)):
            if not self.names[i] is None:
                print('; ' + self.desc[i])
                print(self.names[i])
                print(self.src[i])
                print()

    def saveused(self):
        for i in range(0, len(self.names)):
            if self.used[i] and not self.names[i] is None:
                print('; ' + self.desc[i])
                print(self.names[i])
                print(self.src[i])
                print()

if __name__ == "__main__":
    p = sass()
    p.clearused()
    p.setused(33)
    p.saveused()

