#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>
#include "LynxSD.h"

extern void HandyMusic_Init ();
extern void HandyMusic_PlayMusic ();
extern void HandyMusic_Pause ();
extern void HandyMusic_UnPause ();
extern void HandyMusic_Main ();
extern void HandyMusic_StopAll();
extern void HandyMusic_StopMusic ();
extern void HandyMusic_StopSoundEffect (unsigned char priority);
extern void __fastcall__ HandyMusic_LoadPlayBGM(unsigned char filenr);
extern unsigned __fastcall__ lnx_eeprom_read (unsigned char cell);
extern void __fastcall__ lnx_eeprom_write (unsigned char cell, unsigned val);
extern unsigned char bgmusic;
extern unsigned char currentmusic;
extern unsigned char halted;
extern unsigned char reset;

static const char SDSavePath[] = "/saves/onduty.sav";
extern signed char SDCheck;	// tracks if there is a SD cart with saves enabled. -1 = No check yet, 0 = no SD saves, 1 = SD saves possible 
extern unsigned char invulnerable;

extern unsigned char checkInput ();
extern unsigned char startcredits;

extern int KEYHANDLER_FILENR;
extern int HM_FILENR;
extern int SFX_FILENR;
extern int MUSIC_FILENR;
extern int TILE_FILENR;

static char black_pal[] = {
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,
    0x0000 >> 8,

    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
    0x0000 & 0xff,
};

static char title_pal[] = {
    0x0fff >> 8,
    0x0000 >> 8,
    0x0220 >> 8,
    0x090e >> 8,
    0x0d5d >> 8,
    0x000f >> 8,
    0x0947 >> 8,
    0x0749 >> 8,
    0x0120 >> 8,
    0x0545 >> 8,
    0x0507 >> 8,
    0x0405 >> 8,
    0x0878 >> 8,
    0x08a8 >> 8,
    0x0bd9 >> 8,
    0x0685 >> 8,

    0x0fff & 0xff,
    0x0000 & 0xff,
    0x0220 & 0xff,
    0x090e & 0xff,
    0x0d5d & 0xff,
    0x000f & 0xff,
    0x0947 & 0xff,
    0x0749 & 0xff,
    0x0120 & 0xff,
    0x0545 & 0xff,
    0x0507 & 0xff,
    0x0405 & 0xff,
    0x0878 & 0xff,
    0x08a8 & 0xff,
    0x0bd9 & 0xff,
    0x0685 & 0xff,
};

extern unsigned char check_eeprom;

extern unsigned char HandyMusic_BGMPlaying;
#pragma zpsym("HandyMusic_BGMPlaying");
extern unsigned char HandyMusic_Active;
#pragma zpsym("HandyMusic_Active");

void HandyMusic_Init ();
void HandyMusic_PlayMusic ();
void HandyMusic_UnPause ();
void HandyMusic_Main ();

void initsystem()
{
    lynx_load((int)&KEYHANDLER_FILENR);
    lynx_load((int)&SFX_FILENR);
    lynx_load((int)&HM_FILENR);
    tgi_install(&tgi_static_stddrv); // This will activate the Lynx screen 
    joy_install(&joy_static_stddrv); // This will activate the Lynx joypad
    tgi_init();
    tgi_setpalette(black_pal);
    tgi_setframerate(60);
    HandyMusic_Init();
    CLI();
    while (tgi_busy()) ;
    tgi_init();
    while (tgi_busy()) ;
}

static void init_eeprom() {
}

static unsigned char endgame() {
    return 1;
}

extern void setbtn(signed char row, signed char col, unsigned char color);

extern void drawpad();

void intro(void) 
{
    clock_t now = clock();

    HandyMusic_StopSoundEffect(236); // Chopper
    // Initialize scores from eeprom
    if (check_eeprom == 1) {
        if (SDCheck == -1) {
            FRESULT res;
            res = LynxSD_OpenFileTimeout (SDSavePath);
            if (res == FR_OK) {
	        SDCheck = 2;
                LynxSD_CloseFile();
            } else {
                if (res == FR_DISK_ERR) {
	            SDCheck = 0;
	        } else {
	            SDCheck = -2;
                }
	    }
        }
        if (SDCheck == 1) {
            if (LynxSD_OpenFile(SDSavePath) == FR_OK) {
                //LynxSD_ReadFile((void*)&completedmissions[0], 128);
                LynxSD_CloseFile();
            }
        } else {
            if (SDCheck == 0) {
                //for (i = 0; i < 64; i++) {
                //    unsigned int *buf = (unsigned int*)&completedmissions[0];
                //    buf[i] = lnx_eeprom_read (i);
                //}
            }
        }
	// Does the eeprom contain sensible data?
        check_eeprom = 0;
    }
    // Erase eeprom
    if (check_eeprom == 3) {
        init_eeprom();
	check_eeprom = 2;
    }
    // Save scores to eeprom
    if (check_eeprom == 2) {
        if (SDCheck == 1) {
            if (LynxSD_OpenFile(SDSavePath) == FR_OK) {
                //LynxSD_WriteFile((void*)&completedmissions[0], 128);
                LynxSD_CloseFile();
            }
        }
        if (SDCheck == 0) {
            int i;
            for (i = 0; i < 64; i++) {
                //unsigned int *buf = (unsigned int*)&completedmissions[0];
                //lnx_eeprom_write (i, buf[i]);
            }
        }
        check_eeprom = 0;
    }
    HandyMusic_StopAll();
    if (bgmusic) {
        //currentmusic = 0;
        //HandyMusic_LoadPlayBGM(currentmusic);
    }
} 

