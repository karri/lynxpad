'''
    The playbeat instruments are grouped into 6
    categories

    8 percussion instruments
      Kick
      Bass_Drum
    8 string instruments
    8 solo instruments
    8 slides
    8 computer sounds
'''
import os
from kivy.core.audio import SoundLoader
import time
from sndlib import sndlib

class playbeat:
    def __init__(self):
        self.sndlib = sndlib()
        self.names = self.sndlib.sndnames().copy()
        self.players = []
        for s in self.names:
            if s is None:
                self.players.append(None)
            else:
                fname = s + '.wav'
                if os.path.exists(fname):
                    self.players.append(SoundLoader.load(fname))
                else:
                    self.players.append(None)

    def play(self, sndnr, pitch):
        ''' 0 -> 1, 13 -> 2 '''
        if pitch >= 0:
            multiplyer = (pitch + 13)/13
        else:
            multiplyer = 13 / (-pitch + 13)
        print(sndnr, self.names[sndnr], pitch)
        player = self.players[sndnr]
        if player is None:
            return
        player.pitch = multiplyer
        player.play()

    def stop(self, sndnr):
        player = self.players[sndnr]
        if player is None:
            return
        player.stop()

    def nrofsnds(self):
        return len(self.players)

if __name__ == "__main__":
    p = playbeat()
    for i in range(0, p.nrofsnds()):
        p.play(i, 0)
        time.sleep(1)
        p.stop(i)
    for i in range(-13, 27):
        p.play(16, i)
        time.sleep(0.4)
        p.stop(16)
