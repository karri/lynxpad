	.include	"lynx.inc"
	.include	"zeropage.inc"
	.include	"extzp.inc"
        .import		_openn
        .import		_lynx_load
        .import		lynxskip0
        .import		lynxblock
        .interruptor	_HandyMusic_Main
        .interruptor	_HandyMusic_PCMMain,15
	.export		_HandyMusic_Init
	.export		_HandyMusic_PlayMusic
	.export		_HandyMusic_Active
	.export		_HandyMusic_BGMPlaying
	.export		_HandyMusic_UnPause
	.export		_HandyMusic_Pause
	.export		_HandyMusic_StopMusic
	.export		_HandyMusic_LoadPlayBGM
	.export		_HandyMusic_StopAll
	.export		_HandyMusic_StopSoundEffect
	.export		_PlayPCMSample
	.export		_HandyMusic_PlaySFX
	.export		_Sample_Playing: zp
	.import		__SOUND_EFFECTS__
	.import		__SOUND_EFFECTS1__
	.import		__SOUND_EFFECTS2__
	.import		_MUSIC_FILENR
	.import		_SAMPLES_FILENR

;****************************************************************
;		  HandyMusic- Main Driver Source		*
;****************************************************************
;HandyMusic lives from $A000-$A6AF
;Music Data lives from $B000-$BFFF
;Sound effects may be placed anywhere, but are
;usually packed after the HandyMusic engine and before the music.
;****************************************************************
;		HandyMusic- Memory Layout/Defines		*
;****************************************************************
; All of the ZeroPage Variables for HandyMusic are listed
; below with a description of their function.
;****************************************************************
;			  Zeropage Vars				*
;****************************************************************
;***********************
; General/Flow Control *
;***********************
;FileNum_MusicBase .set _MUSIC_FILENR
;FileNum_SampleBase .set _SAMPLES_FILENR

.segment "APPZP": zeropage
HandyMusic_Enable:	.res 1
; Enable (!0) HandyMusic processing.
; This is enabled after initialization, and disabled during pause.

_HandyMusic_Active:	.res 1
; HandyMusic is currently processing data if this is non-zero.
; Used to check for double-entry into the driver.

_HandyMusic_BGMPlaying:	.res 1
; Simple boolean flag to indicate if the background music
; is playing or not.
;***************
; Pause Backup *
;***************
HandyMusic_Pause_TimerBack:	.res 4
HandyMusic_Pause_VolumeBack:	.res 4
; Backup values of the timer and volume registers used
; when HandyMusic is paused or unpaused.
;***************
; For Channels *
;***************
HandyMusic_Channel_NoWriteBack: 	.res 4
; Disables writing any data to the respective channel
; if non-zero. All frequency calculations, etc. are 
; still performed, but writes are disabled until the
; setting is zeroed again. Useful for grabbing channels
; for sample playback, etc.

HandyMusic_SFX_AddressTableLoLo:	.res 1
HandyMusic_SFX_AddressTableLoHi:	.res 1
HandyMusic_SFX_AddressTableHiLo:	.res 1
HandyMusic_SFX_AddressTableHiHi:	.res 1
HandyMusic_SFX_AddressTablePriLo:	.res 1
HandyMusic_SFX_AddressTablePriHi:	.res 1
; The pointers to three tables, each respectively 
; containing the low address, high address, and priority
; of the sound effect scipts

HandyMusic_SFX_EnqueueNext:	.res 1
HandyMusic_SFX_PlayRequest:	.res 1
; If a sound effect needs to be added on the next frame,
; its number will be located here.

HandyMusic_Channel_DecodePointer:	.res 2
; The 16-bit pointer used to decode instruments and sound
; effects. This is shared by any Instruments or SFX playing
; as each takes a turn being decoded by the driver.

HandyMusic_Channel_Priority: 	.res 4
; The current priorty of the four audio channels in the
; Lynx. A priority of 0 is considered inactive (no sound),
; if a sound effect is playing on the channel, its current priority
; will be respectively stored here, if the channel is in use by 
; the background music driver, the current Instrument's
; priority will be stored here.
; Please note that if a SFX or Instrument sets its priority to zero
; it will never play, as this flag is also used during channel update
; decoding.

HandyMusic_Channel_LoopDepth:	.res 4
; The current depth into the sound effect and instrument loop points.

HandyMusic_Channel_FinalFreqLo:	.res 1
HandyMusic_Channel_FinalFreqHi:	.res 1
; A temporary variable used to hold the final calculated frequency
;********************
; For Music Scripts *
;********************
HandyMusic_Instrument_AddrTableLoLo:	.res 1
HandyMusic_Instrument_AddrTableLoHi:	.res 1
HandyMusic_Instrument_AddrTableHiLo:	.res 1
HandyMusic_Instrument_AddrTableHiHi:	.res 1
; The pointers to three tables, each respectively 
; containing the low address, high address, and priority
; of the instrument scipts

HandyMusic_Music_DecodePointer:	.res 2
; The 16-bit pointer used to decode the music script. This
; is shared by the four music tracks, each takes a turn
; running through the script, copying its own current decode
; address here.

HandyMusic_Music_Priority: 	.res 4
; The current priorty of the four music tracks, used to check
; if the note still has control of the channel. A value of zero
; indicates that decoding has stopped for this channel.

HandyMusic_Music_LoopDepth:	.res 4
; The current depth into the music track loop points.
;******************
; Sample Playback *
;******************
_Sample_Playing: .res 1
; Nonzero if a sample is playing

Sample_PanBackup: .res 1
; Backup for the channel panning during sample playback

.segment "HM_CODE"
;**********************
; Some Useful Equates *
;**********************
Lynx_Audio_Ch0		.set 0
Lynx_Audio_Ch1		.set 8
Lynx_Audio_Ch2		.set 16
Lynx_Audio_Ch3		.set 24

Lynx_Audio_Volume	.set $FD20
Lynx_Audio_FeedBackReg	.set $FD21
Lynx_Audio_DirectVol	.set $FD22
Lynx_Audio_ShiftRegLo	.set $FD23
Lynx_Audio_TimerBack	.set $FD24
Lynx_Audio_TimerCont	.set $FD25
Lynx_Audio_AudioExtra	.set $FD27

Lynx_Audio_Atten_0	.set $FD40
Lynx_Audio_Atten_1	.set $FD41
Lynx_Audio_Atten_2	.set $FD42
Lynx_Audio_Atten_3	.set $FD43
Lynx_Audio_Panning	.set $FD44
Lynx_Audio_Stereo	.set $FD50

HandyMusic_Song_BaseAddress	.set $B000
HandyMusic_Song_Priorities	.set HandyMusic_Song_BaseAddress + 0
HandyMusic_Song_TrackAddrLo	.set HandyMusic_Song_BaseAddress + 4
HandyMusic_Song_TrackAddrHi	.set HandyMusic_Song_BaseAddress + 8
HandyMusic_Song_InstrLoLo	.set HandyMusic_Song_BaseAddress + 12
HandyMusic_Song_InstrLoHi	.set HandyMusic_Song_BaseAddress + 13
HandyMusic_Song_InstrHiLo	.set HandyMusic_Song_BaseAddress + 14
HandyMusic_Song_InstrHiHi	.set HandyMusic_Song_BaseAddress + 15
;****************************************************************
;	HandyMusic- Instrument/SFX Script Decode Commands	*
;****************************************************************
;Command Descriptions-						*
;								*
;0: Stop Script Decoding					*
;	Format: [0] (1 byte)					*
;		Immediately stops the decoding of 		*
;		the sound script and frees the channel.		*
;1: Wait							*
;	Format: [1][Number of Frames] (2 bytes)			*
;		Pauses sound script decoding for the 		*
;		specified number of frames.			*
;								*
;2: Set Shift, Feedback, and Integrate Mode			*
;	Format: [2][ShiftReg Lo][ShiftReg Hi][Feedback Lo]	*
;		[Feedback Hi + Integrate Flag] (5 bytes)	*
;		Replaces the current shift and feedback 	*
;		register contents with the specified new 	*
;		values, stored in the same format as in the 	*
;		instrument/sfx header.				*
;								*
;3: Set Volume and Volume Adjustment				*
;	Format: [3][Volume][Volume Adjustment]			*
;		[Volume Adjustment Decimal] (4 bytes)		*
;		Replaces the current volume and volume		*
;		adjustment values with the specified new values.*
;								*
;4: Set Frequency Offset and Frequency Offset Adjustment	*
;	Format: [4][Frequency Offset Lo][Frequency Offset Hi]	*
;		[Frequency Offset Adjustment Lo]		*
;		[Frequency Offset Adjustment Hi]		*
;		[Frequency Offset Adjustment Decimal] (6 bytes)	*
;		Replaces the current frequency offset and 	*
;		frequency offset adjustment values with 	*
;		the specified new ones.				*
;								*
;5: Set Loop Point						*
;	Format: [5][Number of Times to Loop] (2 bytes)		*
;		Defines the location in the script following 	*
;		this command as a loop point which will be 	*
;		returned to the specified number of times. If 	*
;		a negative number is used, the loop will 	*
;		continue infinitely.				*
;								*
;6: Loop							*
;	Format: [6] (1 byte)					*
;		If the loop is not infinite, the loop counter 	*
;		is decremented and unless the counter has 	*
;		reached zero, the script decoder will continue 	*
;		decoding at the loop point.			*
;		Loops may be four-deep.				*
;****************************************************************
.segment "HM_RODATA"
HandyMusic_SFX_CommandTableLo:
	.byte <HandyMusic_FreeChannel,<HandyMusic_SFX_Wait,<HandyMusic_SFX_SetSFB
	.byte <HandyMusic_SFX_SetVolume,<HandyMusic_SFX_SetFrequency
	.byte <HandyMusic_SFX_SetLoop,<HandyMusic_SFX_Loop
HandyMusic_SFX_CommandTableHi:
	.byte >HandyMusic_FreeChannel,>HandyMusic_SFX_Wait,>HandyMusic_SFX_SetSFB
	.byte >HandyMusic_SFX_SetVolume,>HandyMusic_SFX_SetFrequency
	.byte >HandyMusic_SFX_SetLoop,>HandyMusic_SFX_Loop
;****************************************************************
; HandyMusic_SFX_GetBytes:					*
;	Gets the next byte of the SFX script, returns in A.	*
;****************************************************************
.segment "HM_CODE"
.proc HandyMusic_SFX_GetBytes: near
	lda (HandyMusic_Channel_DecodePointer)
	INC HandyMusic_Channel_DecodePointer
	bne norollover
	INC HandyMusic_Channel_DecodePointer + 1
norollover:
	rts
.endproc
;****************************************************************
; HandyMusic_SFX_Wait:						*
;	Sets a delay which is waited upon before decoding any	*
; other script commands.					*
;****************************************************************
HandyMusic_SFX_Wait:
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_DecodeDelay,X
	rts
;****************************************************************
; HandyMusic_SFX_SetSFB:					*
;	Sets the shift and feedback registers, the channel is	*
; also shut off during this time. But it will be turned back	*
; on during the frequency/volume envelope calculation period.	*
;****************************************************************
HandyMusic_SFX_SetSFB:
	LDY HandyMusic_Redirect_ChOffs,X
	LDA#0
	STA Lynx_Audio_TimerCont,Y		; Disable Counter
	DEC
	STA HandyMusic_Channel_ForceUpd,X	; Force update channel.

	jsr HandyMusic_SFX_GetBytes
	STA Lynx_Audio_ShiftRegLo,Y		; Copy low bits of shift register
	jsr HandyMusic_SFX_GetBytes
	STA Lynx_Audio_AudioExtra,Y		; Copy high bits of shift register
	jsr HandyMusic_SFX_GetBytes
	STA Lynx_Audio_FeedBackReg,Y		; Copy feedback bits
	jsr HandyMusic_SFX_GetBytes
	STA Lynx_Audio_TimerCont,Y		; Copy Feedback bit 7 and integrate

	rts ; That's all, folks. The channel is off for a bit, but that should be ok.
;****************************************************************
; HandyMusic_SFX_SetVolume:					*
;	Sets the Volume and Volume Envelope for the channel.	*
;****************************************************************
HandyMusic_SFX_SetVolume:
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_Volume,X
	STZ HandyMusic_Channel_VolumeDec,X ; Copy Volume, clear decimal

	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_VolumeAdjust,X
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_VolumeAdjustDec,X ; Copy Volume Adjustment & Decimal
	rts
;****************************************************************
; HandyMusic_SFX_SetFrequency:					*
;	Sets the Frequency and Frequency Envelope for the	*
; channel.							*
;****************************************************************
HandyMusic_SFX_SetFrequency:
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_FreqOffsetLo,X
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_FreqOffsetHi,X
	STZ HandyMusic_Channel_FreqOffsetDec,X ; Copy frequency Lo/Hi, clear decimal

	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_OffsetPitAdjLo,X
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_OffsetPitAdjHi,X
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_OffsetPitAdjDec,X ; Copy frequency Adjustment Lo,Hi,Dec

	LDA#$FF
	STA HandyMusic_Channel_ForceUpd,X	; Force update channel.
	rts
;****************************************************************
; HandyMusic_SFX_SetLoop:					*
;	Sets the current script decoding address as a loop	*
; point, increases the current loop depth, and copies the loop	*
; count.							*
;****************************************************************
HandyMusic_SFX_SetLoop:
	INC HandyMusic_Channel_LoopDepth,X ; Increase loop depth
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X ; Calculate depth into loop arrays
	ADC HandyMusic_Channel_LoopDepth,X
	TAY
	jsr HandyMusic_SFX_GetBytes ; Get the number of times to loop
	STA HandyMusic_Channel_LoopCount-1,Y ; Store in the loop count (offset by -1)
	LDA HandyMusic_Channel_DecodePointer
	STA HandyMusic_Channel_LoopAddrLo-1,Y
	LDA HandyMusic_Channel_DecodePointer + 1
	STA HandyMusic_Channel_LoopAddrHi-1,Y ; Store the current decode pointer
	rts ; That's it.
;****************************************************************
; HandyMusic_SFX_Loop:						*
;	Checks to see if a loop condition is valid, and loops	*
; if it is.							*
;****************************************************************
HandyMusic_SFX_Loop:
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X ; Calculate depth into loop arrays
	ADC HandyMusic_Channel_LoopDepth,X
	TAY
	LDA HandyMusic_Channel_LoopCount-1,Y ; If negative, this is an infinite loop
	bmi loop
	DEC
	STA HandyMusic_Channel_LoopCount-1,Y ; Decrement count, if zero, the loop fails
	beq noloop
loop:
	LDA HandyMusic_Channel_LoopAddrLo-1,Y
	STA HandyMusic_Channel_DecodePointer
	LDA HandyMusic_Channel_LoopAddrHi-1,Y
	STA HandyMusic_Channel_DecodePointer + 1 ; Pull old decode address
	rts
noloop:
	DEC HandyMusic_Channel_LoopDepth,X ; Decrement loop depth, we're done.
	rts


;****************************************************************
;	    HandyMusic- Music Script Decode Commands		*
;****************************************************************
;Command Descriptions-						*
;								*
;0: Set Priority						*
;	Format: [0][Priority] (2 bytes)				*
;		Sets the priority of the track relative to 	*
;		the sound effects. Higher priorities win out 	*
;		when competing for channels, the same 		*
;		priorities should never be used between sound 	*
;		effects and instruments. A priority of zero 	*
;		stops the track from decoding.			*
;								*
;1: Set Panning							*
;	Format: [1][Panning] (2 bytes)				*
;		Sets the panning of the instruments 		*
;		played in the current channel.			*
;								*
;2: Note On							*
;	Format: [2][Instrument][Base Frequency Lo]		*
;		[Base Frequency Hi][Delay Lo] (5 bytes)		*
;		Plays a given instrument with the specified 	*
;		base frequency, then waits for the given 	*
;		one byte delay					*
;								*
;3: Note Off							*
;	Format: [3][Delay Lo] (2 bytes)				*
;		Forces the currently playing instrument into 	*
;		the note off portion of its script, then waits 	*
;		for the specified one byte delay.		*
;								*
;4: Set Base Frequency Adjustment				*
;	Format: [4][Base Frequency Adjustment Lo]		*
;		[Base Frequency Adjustment Hi]			*
;		[Base Frequency Adjustment Dec] (4 bytes)	*
;		Sets the Base Frequency Adjustment value, 	*
;		which can be used for pitch slides, etc. 	*
;		This value is initialized to zero on the 	*
;		start of a song.				*
;								*
;5: Set Loop Point						*
;	Format: [5][Number of Times to Loop] (2 bytes)		*
;		Defines the location in the script following 	*
;		this command as a loop point which will be 	*
;		returned to the specified number of times. If 	*
;		a negative number is used, the loop will 	*
;		continue infinitely.				*
;								*
;6: Loop							*
;	Format: [6] (1 byte)					*
;		If the loop is not infinite, the loop counter 	*
;		is decremented and unless the counter has 	*
;		reached zero, the script decoder will continue 	*
;		decoding at the loop point.			*
;		Loops may be four-deep.				*
;								*
;7: Wait							*
;	Format: [7][Delay Lo][Delay Hi] (3 bytes)		*
;		Stops decoding for the specified two byte delay.*
;								*
;8: Play Sample							*
;	Format: [8][Sample Number] (2 bytes)			*
;		Plays back the specified PCM sample in 		*
;		channel 0. Note that commands continue to 	*
;		process while the sample is played, so delays 	*
;		have to be added manually. Only call this	*
;		command in Track 0.				*
;								*
;9: Pattern Call						*
;	Format: [9][Address Lo][Address Hi] (3 Bytes)		*
;		Jumps to the address of the music script given,	*
;		and sets up the current position in the music	*
;		script as the destination for the Pattern	*
;		Return command.					*
;								*
;A: Pattern Return						*
;	Format: [A] (1 byte)					*
;		Returns to the portion of the music script	*
;		which was being played before the last Pattern	*
;		Call command.					*
;								*
;B: Short Note On						*
;	Format: [B][Base Frequency Lo][Base Frequency Hi]	*
;		[Delay Lo] (4 bytes)				*
;		Plays the last used instrument with the 	*
;		specifiedbase frequency, then waits for 	*
;		the given one byte delay.			*
;C: Pattern Break						*
;	Format: [C] (1 byte)					*
;		Returns all channels to the lowest return	*
;		address found in their pattern call stack.	*
;		This effectively returns all channels to the	*
;		main script (all of them- not just the		*
;		currently decoding channel).			*
;****************************************************************
.segment "HM_RODATA"
HandyMusic_Mus_CommandTableLo:
	.byte <HandyMusic_Mus_SetPri,<HandyMusic_Mus_SetPan
	.byte <HandyMusic_Mus_NoteOn,<HandyMusic_Mus_NoteOff
	.byte <HandyMusic_Mus_SetFreqAdj,<HandyMusic_Music_SetLoop
	.byte <HandyMusic_Mus_Loop,<HandyMusic_Mus_Wait
	.byte <HandyMusic_Mus_Sampl,<HandyMusic_Mus_Call
	.byte <HandyMusic_Mus_Ret,<HandyMusic_Mus_SNoteOn
	.byte <HandyMusic_Mus_Break
HandyMusic_Mus_CommandTableHi:
	.byte >HandyMusic_Mus_SetPri,>HandyMusic_Mus_SetPan
	.byte >HandyMusic_Mus_NoteOn,>HandyMusic_Mus_NoteOff
	.byte >HandyMusic_Mus_SetFreqAdj,>HandyMusic_Music_SetLoop
	.byte >HandyMusic_Mus_Loop,>HandyMusic_Mus_Wait
	.byte >HandyMusic_Mus_Sampl,>HandyMusic_Mus_Call
	.byte >HandyMusic_Mus_Ret,>HandyMusic_Mus_SNoteOn
	.byte >HandyMusic_Mus_Break
.segment "HM_CODE"
;****************************************************************
; HandyMusic_Mus_GetBytes:					*
;	Gets the next byte of the SFX script, returns in A.	*
;****************************************************************
HandyMusic_Mus_GetBytes:
	LDA (HandyMusic_Music_DecodePointer)
	INC HandyMusic_Music_DecodePointer
	bne norollover2
	INC HandyMusic_Music_DecodePointer + 1
norollover2:
	rts
;****************************************************************
; HandyMusic_Mus_SetPri:					*
;	Sets the priority of the music script to a new specified*
; value, takes effect immediately. A priority setting of zero	*
; will effectively disable the channel. Don't change the 	*
; priority of a channel in between a note on and off.		*
;****************************************************************
HandyMusic_Mus_SetPri:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_Priority,X
	rts
;****************************************************************
; HandyMusic_Mus_SetPan:					*
;	Sets the stereo panning of the current channel, taking	*
; effect when the next note on command is reached. Format of	*
; panning is LLLLRRRR where 0000 is silent and 1111 is max.	*
;****************************************************************
HandyMusic_Mus_SetPan:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Channel_Panning,X
	rts
;****************************************************************
; HandyMusic_Mus_NoteOn:					*
; HandyMusic_Mus_SNoteOn:					*
;	Plays a note on a given instrument at the specified 	*
; base frequency with the previously given panning and		*
; frequency adjustment values. Then waits on a one byte delay.	*
;****************************************************************
HandyMusic_Mus_NoteOn:
	jsr HandyMusic_Mus_GetBytes ; Get instrument # in A,Y
	STA HandyMusic_Music_LastInstrument,X
HandyMusic_Mus_SNoteOn:
	LDA HandyMusic_Music_LastInstrument,X
	TAY
	LDA HandyMusic_Music_Priority,X ; See if music is still in control of
	CMP HandyMusic_Channel_Priority,X ; the channel. If so, play the note.
	bcs playnote00
	jsr HandyMusic_Mus_GetBytes ; Otherwise skip the playing and just set up
	jsr HandyMusic_Mus_GetBytes ; the delay.
	bra playnote10
playnote00:
	STA HandyMusic_Channel_Priority,X ; Set channel priority to music priority
	STZ HandyMusic_Channel_LoopDepth,X ; Reset Loop Depth
	STZ HandyMusic_Channel_DecodeDelay,X ; And decode delay
	STZ HandyMusic_Channel_BaseFreqDec,X
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Channel_BaseFreqLo,X
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Channel_BaseFreqHi,X ; Copy passed frequency

	LDA HandyMusic_Music_BasePitAdjLo,X
	STA HandyMusic_Channel_BasePitAdjLo,X
	LDA HandyMusic_Music_BasePitAdjHi,X
	STA HandyMusic_Channel_BasePitAdjHi,X
	LDA HandyMusic_Music_BasePitAdjDec,X
	STA HandyMusic_Channel_BasePitAdjDec,X ; Then set up frequency adjustment value

	LDA (HandyMusic_Instrument_AddrTableLoLo),Y ; Copy script pointer to work pointer
	STA HandyMusic_Channel_DecodePointer
	LDA (HandyMusic_Instrument_AddrTableHiLo),Y
	STA HandyMusic_Channel_DecodePointer + 1

	jsr HandyMusic_Enqueue_IS ; Shared NoteOff, SFB, Vol, Freq in HandyMusic_EnqueueSFX

	LDA HandyMusic_Channel_NoWriteBack,X ; Writing disabled?
	bne playnote10
	LDA HandyMusic_Channel_Panning,X
	STA Lynx_Audio_Atten_0,X ; Set up panning
playnote10:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_DecodeDelayLo,X
	STZ HandyMusic_Music_DecodeDelayHi,X ; Set up one byte delay
	rts ; We're done
;****************************************************************
; HandyMusic_Mus_NoteOff:					*
;	Forces a note into the note off portion of its script	*
; if it still has control of the channel, then waits for a	*
; one byte delay. If the note has lost control of the channel	*
; only the delay is instated.					*
;****************************************************************
HandyMusic_Mus_NoteOff:
	LDA HandyMusic_Music_Priority,X ; See if music is still in control of
	CMP HandyMusic_Channel_Priority,X ; the channel. If not, just instate delay.
	bne instatedelay
	LDA HandyMusic_Channel_NoteOffPLo,X
	STA HandyMusic_Channel_DecodePointerLo,X
	LDA HandyMusic_Channel_NoteOffPHi,X
	STA HandyMusic_Channel_DecodePointerHi,X ; Relocate decode pointer to note off
	STZ HandyMusic_Channel_DecodeDelay,X	; Force decode to happen
instatedelay:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_DecodeDelayLo,X
	STZ HandyMusic_Music_DecodeDelayHi,X ; Set up one byte delay
	rts
;****************************************************************
; HandyMusic_Mus_SetFreqAdj:					*
;	Sets the frequency adjustment value for the current	*
; music track. This is useful for pitch bends, etc.		*
;****************************************************************
HandyMusic_Mus_SetFreqAdj:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_BasePitAdjLo,X
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_BasePitAdjHi,X
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_BasePitAdjDec,X
	rts
;****************************************************************
; HandyMusic_Music_SetLoop:					*
;	Sets the current script decoding address as a loop	*
; point, increases the current loop depth, and copies the loop	*
; count.							*
;****************************************************************
HandyMusic_Music_SetLoop:
	INC HandyMusic_Music_LoopDepth,X ; Increase loop depth
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X ; Calculate depth into loop arrays
	ADC HandyMusic_Music_LoopDepth,X
	TAY
	jsr HandyMusic_Mus_GetBytes ; Get the number of times to loop
HandyMusic_Music_LoopPush:
	STA HandyMusic_Music_LoopCount-1,Y ; Store in the loop count (offset by -1)
	LDA HandyMusic_Music_DecodePointer
	STA HandyMusic_Music_LoopAddrLo-1,Y
	LDA HandyMusic_Music_DecodePointer + 1
	STA HandyMusic_Music_LoopAddrHi-1,Y ; Store the current decode pointer
	rts ; That's it.
;****************************************************************
; HandyMusic_Mus_Loop:						*
;	Checks to see if a loop condition is valid, and loops	*
; if it is.							*
;****************************************************************
HandyMusic_Mus_Loop:
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X ; Calculate depth into loop arrays
	ADC HandyMusic_Music_LoopDepth,X
	TAY
	LDA HandyMusic_Music_LoopCount-1,Y ; If negative, this is an infinite loop
	bmi loop2
	DEC
	STA HandyMusic_Music_LoopCount-1,Y ; Decrement count, if zero, the loop fails
	beq noloop2
loop2:
	LDA HandyMusic_Music_LoopAddrLo-1,Y
	STA HandyMusic_Music_DecodePointer
	LDA HandyMusic_Music_LoopAddrHi-1,Y
	STA HandyMusic_Music_DecodePointer + 1 ; Pull old decode address
	rts
noloop2:
	DEC HandyMusic_Music_LoopDepth,X ; Decrement loop depth, we're done.
	rts
;****************************************************************
; HandyMusic_Mus_Wait:						*
;	Waits on a two byte delay.				*
;****************************************************************
HandyMusic_Mus_Wait:
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_DecodeDelayLo,X
	jsr HandyMusic_Mus_GetBytes
	STA HandyMusic_Music_DecodeDelayHi,X
	rts
;****************************************************************
; HandyMusic_Mus_Sampl:					*
;	Grabs Channel 0 and plays back the selected sample.	*
; Note that commands continue to process while the sample is	*
; played, so delays have to be added manually.			*
;****************************************************************
HandyMusic_Mus_Sampl:
	PHX
	jsr HandyMusic_Mus_GetBytes
	jsr _PlayPCMSample
	PLX
	rts
;****************************************************************
; HandyMusic_Mus_Call:						*
;	"JSR" to the specified location in the sound script.	*
; Note that this is logged as an infinite loop.			*
;****************************************************************
HandyMusic_Mus_Call:
	jsr HandyMusic_Mus_GetBytes ; Fetch Pattern Address Lo
	PHA
	jsr HandyMusic_Mus_GetBytes ; Fetch Pattern Address Hi
	PHA
	INC HandyMusic_Music_LoopDepth,X ; Increase loop depth
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X ; Calculate depth into loop arrays
	ADC HandyMusic_Music_LoopDepth,X
	TAY
	LDA#$80
	jsr HandyMusic_Music_LoopPush

	PLA
	STA HandyMusic_Music_DecodePointer + 1
	PLA
	STA HandyMusic_Music_DecodePointer ; Set decode pointer to pattern address
	rts
;****************************************************************
; HandyMusic_Mus_Ret:						*
;	"rts" from the current location in the sound script.	*
;****************************************************************
HandyMusic_Mus_Ret:
	jsr HandyMusic_Mus_Loop ; "Loop" to return address
	DEC HandyMusic_Music_LoopDepth,X ; But force loop depth to decrement
	rts
;****************************************************************
; HandyMusic_Mus_Break:					*
;	Pattern break, returning all channels to the main	*
; track (lowest CALL found, based upon $80 tags, if any).	*
;****************************************************************
HandyMusic_Mus_Break:
	jsr HandyMusic_BackupDecodePointer ; Temporary decode pointer will get trashed.
	PHX
	LDX#3
checkNextTrack:
	clc
	LDA HandyMusic_Channel_LoopAddrDepth,X
	TAY
	ADC HandyMusic_Music_LoopDepth,X
	STA checkLoopPoint + 1
checkLoopPoint:
	CPY#0		; Comparison value modified by STA above to stack top
	beq trackDone
	LDA HandyMusic_Music_LoopCount,Y
	INY
	CMP#$80		; Any loop count of $80 is a special tag for calls
	beq foundBase	; So if we found one from the bottom up, that's our base call
	bra checkLoopPoint ; Otherwise, check the next entry in the stack
foundBase:
	TYA
	sec
	SBC HandyMusic_Channel_LoopAddrDepth,X
	STA HandyMusic_Music_LoopDepth,X
	jsr HandyMusic_Mus_Ret ; Force a RET on the lowest CALL
	STZ HandyMusic_Music_DecodeDelayLo,X
	STZ HandyMusic_Music_DecodeDelayHi,X ; Also force the track to proceed immediately.
	jsr HandyMusic_BackupDecodePointer ; Then back up the decode pointer
trackDone:
	DEX
	bpl checkNextTrack
	PLX
	LDA HandyMusic_Music_DecodePointerLo,X
	STA HandyMusic_Music_DecodePointer
	LDA HandyMusic_Music_DecodePointerHi,X
	STA HandyMusic_Music_DecodePointer + 1 ; Restore the temporary decode pointer
	rts
;****************************************************************
;	    HandyMusic- Audio Sample Playback Routines		*
;****************************************************************
; All of these functions use the following ZP Vars:		*
;	-Sample_Playing						*
;	-Sample_PanBackup					*
;	-FileBlockHigh						*
;	-CurrBlock						*
;	-BlockByte						*
;	-entry							*
;	-DestPtr						*
;****************************************************************
;****************************************************************
; PlayPCMSample:						*
;	Starts playing a PCM Sample (Sample number in A) by	*
;	capturing channel 0 from HandyMusic, and starting the 	*
;	read stream using timer 3. HandyMusic is given back 	*
;	control of channel 0 once the sample is			*
;	finished playing.					*
;****************************************************************
_PlayPCMSample:
	PHA
	;sei					; Kill IRQs just for a bit to...
	LDA HandyMusic_Disable_Samples
	beq permissionGranted			; Make sure we're allowed to play
	PLA
	;cli
	rts
permissionGranted:
	LDA#$FF
	STA HandyMusic_Channel_NoWriteBack	; Capture Channel 0
	STZ $FD25
	STZ $fD0D				; Disable IRQ3 in case sample is already playing
	;cli
	LDA $FD40
	STA Sample_PanBackup
	LDA #$FF
	STA $FD40
	STA _Sample_Playing			; Sample is playing
	PLA
	clc
;**********************
; Load File Directory *
;**********************
	;ADC#<_SAMPLES_FILENR			; Sample File Offsets
	;ldx #0
	;jsr _openn				; Load Directory
;***********************
; Parse File Directory *
;***********************
ParseDir00:
;	LDA _FileExecFlag			; Check to see if exec flag is set
;	bne ParseDir01				; in order to toggle AUDIN
;	LDA __iodat				; (For 1MB Card control)
;	AND#%11101111
;	STA __iodat
;	bra ParseDir02
;ParseDir01:
;	LDA __iodat
;	ORA#%00010000
;	STA __iodat
;ParseDir02:
	lda _FileStartBlock
	sta _FileCurrBlock                   ; startblock
	jsr lynxblock
	lda _FileBlockOffset
	eor #$FF
	tax
	lda _FileBlockOffset+1
	eor #$FF
	tay
	jsr lynxskip0
;********************
; Setup Timer 3 IRQ *
;********************
	LDA#125
	STA $fd0c; T3 Backup (125)
	STA $fd0e; T3 Current
	LDA#$D8
	STA $fd0d; T3 Mode (~8000Hz Reload)
	rts



;****************************************************************
; PCMSample_IRQ:						*
;	Streams a single sample from the cartridge, playing	*
;	it through the direct volume register of channel 0.	*
;	Automatically disabled when playback is finished.	*
;****************************************************************
PCMSample_IRQ:
;echo "HandyMusic PCM IRQ Address: %HPCMSample_IRQ"
;***************************
;* Read one byte from Cart *
;***************************
	;PHP
	;PHA
	DEC _FileFileLen
	bne KeepReading
	DEC _FileFileLen+1
	beq PlayBackDone
KeepReading:
	jsr ReadByte	; Read byte
	sta $fd22	; Store to Direct Volume reg
	bra ExitIRQ
PlayBackDone:
	STZ $fd0d ; Kill IRQ, Sample is finished.
	STZ _Sample_Playing ; Sample is not playing
	LDA Sample_PanBackup
	STA $FD40
	STZ HandyMusic_Channel_NoWriteBack
ExitIRQ:
	;PLA
	;PLP
	rts
;**********************************************************
;* Fetch one byte from cart, reselecting block if needed. *
;**********************************************************
ReadByte:
;	LDA _FileExecFlag
;	AND#2
;	beq LoROMRead
;	lda $fcb3
;	bra IncCartByte
;LoROMRead:
	lda $fcb2
IncCartByte:
	inc _FileBlockByte
	bne BailOut
	inc _FileBlockByte+1
	bne BailOut
	jmp lynxblock
BailOut:
	rts


;****************************************************************
; HandyMusic_Main:						*
;	The main entry point for the HandyMusic driver, this 	*
; should be called every time you want HandyMusic to process	*
; all music/sfx data and adjust the audio registers. The most	*
; convenient way is to simply tie the routine to VBlank and	*
; run the audio updates at 60Hz. You can also use any of the	*
; available Lynx timers and call HandyMusic through an IRQ.	*
; However, it should be noted that the composition software is	*
; all designed for the 60Hz (fast) mode.			*
;****************************************************************
_HandyMusic_PCMMain:
	lda	INTSET
	and	#TIMER3_INTERRUPT
	beq	@L0
	jsr	PCMSample_IRQ
	sec
	rts
@L0:
	clc
	rts

_HandyMusic_Main:
; Personal notes:
; HandyMusic expects A,X,Y, and P to be preserved before
; being called. Don't forget it!
	lda	INTSET
	and	#VBL_INTERRUPT
	bne	@L1
	clc
	rts
@L1:
	;sei ; Quickly Disable IRQs
	LDA HandyMusic_Enable			; Is HandyMusic enabled?
	bne HandyMusic_CActive			; If not, bail.
	clc
	rts
HandyMusic_CActive:
	LDA _HandyMusic_Active			; Or is it already in a decode
	beq HandyMusic_Decode00			; procedure?
	clc
	rts
HandyMusic_Decode00:
	INC _HandyMusic_Active			; We're in the decode procedure now.
	;cli					; Re-enable interrupts so we don't miss anything.
;**********************
; Channel Update Call *
;**********************
ChannelUpdates00:				; Update all of the channels (SFX/Instrument processing)
	LDX#3
ChannelUpdates01:
	jsr HandyMusic_UpdateChannel
	DEX
	bpl ChannelUpdates01
;********************************************
; Check if a SFX was requested to be played *
;********************************************
SFXPlayCheck00:
	LDA HandyMusic_SFX_PlayRequest
	beq SFXPlayCheck01
	STZ HandyMusic_SFX_PlayRequest
	jsr HandyMusic_EnqueueSFX
SFXPlayCheck01:
;********************
; Music Update Call *
;********************
BGMPlayCheck00:
	LDA _HandyMusic_BGMPlaying
	beq BGMPlayCheck10
	LDX#3
BGMPlayCheck01:
	jsr HandyMusic_DecodeMusic
	DEX
	bpl BGMPlayCheck01
BGMPlayCheck10:
;*************
; Decode End *
;*************
	STZ _HandyMusic_Active			; Done with decoding.
HandyMusic_Exit:
	clc
	rts					; We're done here




;****************************************************************
; HandyMusic_DecodeMusic:					*
;	Decodes the current music script for the channel number	*
; stored in X.							*
;****************************************************************
HandyMusic_DecodeMusic:
	LDA HandyMusic_Music_Priority,X		; is the channel enabled?
	beq return
	LDA HandyMusic_Music_DecodeDelayLo,X
	ORA HandyMusic_Music_DecodeDelayHi,X	; Any Decoding Delays?
	beq MusicDecode00
	LDA HandyMusic_Music_DecodeDelayHi,X
	bne hidelayactive
	DEC HandyMusic_Music_DecodeDelayLo,X	; If so, wait for them.
	bne return
	bra MusicDecode00
hidelayactive:
	LDA HandyMusic_Music_DecodeDelayLo,X
	bne justdeclo
	DEC HandyMusic_Music_DecodeDelayHi,X
justdeclo:
	DEC HandyMusic_Music_DecodeDelayLo,X
return:
	rts
MusicDecode00:
	LDA HandyMusic_Music_DecodePointerLo,X
	STA HandyMusic_Music_DecodePointer
	LDA HandyMusic_Music_DecodePointerHi,X
	STA HandyMusic_Music_DecodePointer + 1	; Copy last decode pointer
MusicDecode01:
	jsr HandyMusic_Mus_GetBytes		; Get current command byte
	TAY
	LDA HandyMusic_Mus_CommandTableLo,Y
	STA MusicDecode12 + 1
	LDA HandyMusic_Mus_CommandTableHi,Y
	STA MusicDecode12 + 2
MusicDecode12:
	jsr $0000				; Process command (destination overwritten)
	LDA HandyMusic_Music_Priority,X		; is the channel enabled?
	beq return
	LDA HandyMusic_Music_DecodeDelayLo,X	; Delay happen?
	ORA HandyMusic_Music_DecodeDelayHi,X
	beq MusicDecode01 ; If not, get next command
HandyMusic_BackupDecodePointer:
	LDA HandyMusic_Music_DecodePointer
	STA HandyMusic_Music_DecodePointerLo,X
	LDA HandyMusic_Music_DecodePointer + 1
	STA HandyMusic_Music_DecodePointerHi,X	; Update decode pointer
	rts					; We're done

;****************************************************************
; HandyMusic_EnqueueSFX:					*
;	Attempts to enqueue a sound effect in an open channel	*
; or one occupied by a lower priority instrument/sfx.		*
;****************************************************************
HandyMusic_EnqueueSFX:
	LDY HandyMusic_SFX_EnqueueNext
	LDX#3
checkzeroes:
	LDA HandyMusic_Channel_Priority,X
	beq foundchannel
	DEX
	bpl checkzeroes
	LDX#3
	LDA (HandyMusic_SFX_AddressTablePriLo),Y
checkpriorities:
	CMP HandyMusic_Channel_Priority,X
	bcs foundchannel
	DEX
	bpl checkpriorities
	rts					; No available Channels
foundchannel:
	LDA (HandyMusic_SFX_AddressTablePriLo),Y ; Get requested SFX priority
	STA HandyMusic_Channel_Priority,X	; Channel found, initialize SFX
	LDA#$FF
	STA Lynx_Audio_Atten_0,X		; Force center panning
	STZ HandyMusic_Channel_LoopDepth,X	; Reset Loop Depth
	STZ HandyMusic_Channel_DecodeDelay,X	; And decode delay

	STZ HandyMusic_Channel_BaseFreqDec,X	; Also clear the base frequency
	STZ HandyMusic_Channel_BaseFreqLo,X	; and pitch adjustment, since those are instrument-only.
	STZ HandyMusic_Channel_BaseFreqHi,X
	STZ HandyMusic_Channel_BasePitAdjDec,X
	STZ HandyMusic_Channel_BasePitAdjLo,X
	STZ HandyMusic_Channel_BasePitAdjHi,X

	LDA (HandyMusic_SFX_AddressTableLoLo),Y ; Copy script pointer to work pointer
	STA HandyMusic_Channel_DecodePointer
	LDA (HandyMusic_SFX_AddressTableHiLo),Y
	STA HandyMusic_Channel_DecodePointer + 1
HandyMusic_Enqueue_IS:
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_NoteOffPLo,X
	jsr HandyMusic_SFX_GetBytes
	STA HandyMusic_Channel_NoteOffPHi,X	; Copy note off pointer

	jsr HandyMusic_SFX_SetSFB
	jsr HandyMusic_SFX_SetVolume
	jsr HandyMusic_SFX_SetFrequency		; Get Shift, Feedback, Volume and Frequency
	LDA HandyMusic_Channel_DecodePointer
	STA HandyMusic_Channel_DecodePointerLo,X
	LDA HandyMusic_Channel_DecodePointer + 1
	STA HandyMusic_Channel_DecodePointerHi,X ; Update decode pointer
	rts					; We're done

;****************************************************************
; HandyMusic_UpdateChannel:					*
;	Performs the general frame updates on the channel	*
; specified by X, such as script decoding, frequency envelopes,	*
; and volume envelopes. A channel is skipped if its priority is	*
; zero.	A is trashed in this routine.				*
;****************************************************************
HandyMusic_UpdateChannel:
	LDA HandyMusic_Channel_Priority,X	; is the channel enabled?
	bne doupdates00
return2:
	LDY HandyMusic_Redirect_ChOffs,X
	STA Lynx_Audio_TimerCont,Y
	rts
doupdates00:
	LDA HandyMusic_Channel_DecodeDelay,X	; Any decode delays active?
	beq doupdates10
	DEC
	STA HandyMusic_Channel_DecodeDelay,X	; If so, decrement and process envelopes
	bra processenvelopes
doupdates10:
	LDA HandyMusic_Channel_DecodePointerLo,X
	STA HandyMusic_Channel_DecodePointer
	LDA HandyMusic_Channel_DecodePointerHi,X
	STA HandyMusic_Channel_DecodePointer + 1 ; Copy last decode pointer
doupdates11:
	jsr HandyMusic_SFX_GetBytes		; Get current command byte
	TAY
	LDA HandyMusic_SFX_CommandTableLo,Y
	STA doupdates12 + 1
	LDA HandyMusic_SFX_CommandTableHi,Y
	STA doupdates12 + 2
doupdates12:
	jsr $0000				; Process command (destination overwritten)
	LDA HandyMusic_Channel_Priority,X	; is the channel enabled?
	beq return2
	LDA HandyMusic_Channel_DecodeDelay,X	; Delay happen?
	beq doupdates11			; If not, get next command

	LDA HandyMusic_Channel_DecodePointer
	STA HandyMusic_Channel_DecodePointerLo,X
	LDA HandyMusic_Channel_DecodePointer + 1
	STA HandyMusic_Channel_DecodePointerHi,X ; Update decode pointer

processenvelopes:
;Base Frequency Update
	clc
	LDA HandyMusic_Channel_BaseFreqDec,X
	ADC HandyMusic_Channel_BasePitAdjDec,X
	STA HandyMusic_Channel_BaseFreqDec,X
	LDA HandyMusic_Channel_BaseFreqLo,X
	ADC HandyMusic_Channel_BasePitAdjLo,X
	STA HandyMusic_Channel_BaseFreqLo,X
	LDA HandyMusic_Channel_BaseFreqHi,X
	ADC HandyMusic_Channel_BasePitAdjHi,X
	STA HandyMusic_Channel_BaseFreqHi,X

;Frequency Offset Update
	clc
	LDA HandyMusic_Channel_FreqOffsetDec,X
	ADC HandyMusic_Channel_OffsetPitAdjDec,X
	STA HandyMusic_Channel_FreqOffsetDec,X
	LDA HandyMusic_Channel_FreqOffsetLo,X
	ADC HandyMusic_Channel_OffsetPitAdjLo,X
	STA HandyMusic_Channel_FreqOffsetLo,X
	LDA HandyMusic_Channel_FreqOffsetHi,X
	ADC HandyMusic_Channel_OffsetPitAdjHi,X
	STA HandyMusic_Channel_FreqOffsetHi,X

;Volume Update
	clc
	LDA HandyMusic_Channel_VolumeDec,X
	ADC HandyMusic_Channel_VolumeAdjustDec,X
	STA HandyMusic_Channel_VolumeDec,X
	LDA HandyMusic_Channel_Volume,X
	ADC HandyMusic_Channel_VolumeAdjust,X
	STA HandyMusic_Channel_Volume,X

;Now Calculate the final frequency
	clc
	LDA HandyMusic_Channel_BaseFreqLo,X
	ADC HandyMusic_Channel_FreqOffsetLo,X
	STA HandyMusic_Channel_FinalFreqLo
	LDA HandyMusic_Channel_BaseFreqHi,X
	ADC HandyMusic_Channel_FreqOffsetHi,X
	STA HandyMusic_Channel_FinalFreqHi

	LDA HandyMusic_Channel_NoWriteBack,X	; Writing disabled?
	beq doWriteback
	rts
doWriteback:
	LDY HandyMusic_Redirect_ChOffs,X
	LDA HandyMusic_Channel_Volume,X
	STA Lynx_Audio_Volume,Y			; Write volume value

	LDA HandyMusic_Channel_ForceUpd,X
	bne updatelastfreq
	LDA HandyMusic_Channel_FinalFreqLo
	CMP HandyMusic_Channel_LastFreqLo,X
	bne updatelastfreq
	LDA HandyMusic_Channel_FinalFreqHi	; Check to see if we need to
	CMP HandyMusic_Channel_LastFreqHi,X	; update the counter at all...
	bne updatelastfreq
	rts
updatelastfreq:
;And perform the register writes
	STZ HandyMusic_Channel_ForceUpd,X
	LDA HandyMusic_Channel_FinalFreqLo
	STA HandyMusic_Channel_LastFreqLo,X
	LDA HandyMusic_Channel_FinalFreqHi
	STA HandyMusic_Channel_LastFreqHi,X

	LDA Lynx_Audio_TimerCont,Y		; Y is set from the volume write above
	AND#%10100000				; Preserve Feedback and Integrate mode switches.
	STA Lynx_Audio_TimerCont,Y		; But shut off channel

	LDA HandyMusic_Channel_FinalFreqHi	; are we using a 1us prescale?
	AND#3
	bne not1usclock

	LDA HandyMusic_Channel_FinalFreqLo
	STA Lynx_Audio_TimerBack,Y		; Write straight frequency divider value
	LDA Lynx_Audio_TimerCont,Y
	ORA#%00011000
	STA Lynx_Audio_TimerCont,Y		; Write prescale and turn channel back on.
	rts
not1usclock:
	LDA HandyMusic_Channel_FinalFreqLo	; Truncate to 7-bits if > 1us prescale
	ORA#$80
	STA Lynx_Audio_TimerBack,Y		; Write frequency divider value

	LDA HandyMusic_Channel_FinalFreqLo
	ROL
	LDA HandyMusic_Channel_FinalFreqHi
	ROL
	DEC
	AND#7
	ORA Lynx_Audio_TimerCont,Y
	ORA#%00011000
	STA Lynx_Audio_TimerCont,Y		; Write prescale and turn channel back on.
	rts

;****************************************************************
; HandyMusic_PlaySFX:						*
;	Set a sound effect to be played on the next audio frame.*
; Sound effect number in A.					*
;****************************************************************
_HandyMusic_PlaySFX:
	PHX
	PHY
	TAX
	LDA HandyMusic_SFX_PlayRequest
	beq SFXisOK
	LDY HandyMusic_SFX_EnqueueNext
	LDA (HandyMusic_SFX_AddressTablePriLo),Y
	PHX
	PLY
	CMP (HandyMusic_SFX_AddressTablePriLo),Y
	bcs BadPriority
SFXisOK:
	STX HandyMusic_SFX_EnqueueNext
	INC HandyMusic_SFX_PlayRequest
BadPriority:
	PLY
	PLX
	rts

;****************************************************************
; HandyMusic_StopSoundEffect:					*
;	Finds the first sound effect with a matching priority	*
; and disables it, it could even be a note! Make sure to use	*
; varying priorities in your notes and sound effects to keep	*
; this routine useful. A contains the priority of the sound	*
; effect or note to disable, X is preserved.			*
;****************************************************************
_HandyMusic_StopSoundEffect:
	PHX
	LDX#3
StopSFX00:
	;sei					; Disable interrupts only for a bit.
	CMP HandyMusic_Channel_Priority,X
	bne StopSFX10
	jsr HandyMusic_FreeChannel
	;cli
	bra exit
StopSFX10:
	;cli					; Now they can be turned back on.
	DEX
	bpl StopSFX00
exit:
	PLX
	rts

;****************************************************************
; HandyMusic_StopAll:						*
;	Stops all playing music tracks and sound effects.	*
;****************************************************************
_HandyMusic_StopAll:
	jsr _HandyMusic_StopMusic		; Stop music
	PHX
	LDX#3
Stopall0:
	;sei					; Disable interrupts only for a bit.
	jsr HandyMusic_FreeChannel		; Force all channels to free
	STZ HandyMusic_SFX_PlayRequest		; Kill SFX Play Request
	;cli					; Now they can be turned back on.
	DEX
	bpl Stopall0
	PLX
	rts

;****************************************************************
; HandyMusic_LoadPlayBGM:					*
;	Loads and Plays the new BGM from the cart in A.		*
;****************************************************************
_HandyMusic_LoadPlayBGM:
	PHA
	jsr _HandyMusic_StopMusic		; Stop Current BGM
waitsampledone:
	LDA _Sample_Playing
	bne waitsampledone
	PLA
	clc
	ADC#<_MUSIC_FILENR
	ldx #0
	jsr _lynx_load				; Load the new BGM, and fall into PlayMusic
;****************************************************************
; HandyMusic_PlayMusic:					*
;	Starts playing the current song loaded into $B000-$BFFF.*
; Make sure to stop playing a song before you load another one.	*
;****************************************************************
_HandyMusic_PlayMusic:
	PHX					; X should be preserved,
	LDX#3					; who knows where we'll be coming from.
PlayMusic00:
	LDA HandyMusic_Song_Priorities,X	; Copy Song Header Data
	STA HandyMusic_Music_Priority,X
	LDA HandyMusic_Song_TrackAddrLo,X
	STA HandyMusic_Music_DecodePointerLo,X
	LDA HandyMusic_Song_TrackAddrHi,X
	STA HandyMusic_Music_DecodePointerHi,X
	STZ HandyMusic_Music_LoopDepth,X
	STZ HandyMusic_Music_DecodeDelayLo,X
	STZ HandyMusic_Music_DecodeDelayHi,X
	STZ HandyMusic_Music_BasePitAdjLo,X
	STZ HandyMusic_Music_BasePitAdjHi,X
	STZ HandyMusic_Music_BasePitAdjDec,X
	DEX
	bpl PlayMusic00
	PLX
	LDA HandyMusic_Song_InstrLoLo
	STA HandyMusic_Instrument_AddrTableLoLo
	LDA HandyMusic_Song_InstrLoHi
	STA HandyMusic_Instrument_AddrTableLoHi
	LDA HandyMusic_Song_InstrHiLo
	STA HandyMusic_Instrument_AddrTableHiLo
	LDA HandyMusic_Song_InstrHiHi
	STA HandyMusic_Instrument_AddrTableHiHi
	INC _HandyMusic_BGMPlaying		; Turn on decoder
	rts

;****************************************************************
; HandyMusic_StopMusic:					*
;	Stops the current music track from decoding, freeing	*
; any channels it may have been using in the process.		*
; A is trashed, X is preserved.					*
;****************************************************************
_HandyMusic_StopMusic:
	STZ _HandyMusic_BGMPlaying
	PHX
	LDX#3
StopMusic00:
	;sei					; Disable interrupts only for a bit to free the channel
	LDA HandyMusic_Music_Priority,X		; If the priorities are identical, it's a note
	CMP HandyMusic_Channel_Priority,X
	bne StopMusic10
	jsr HandyMusic_FreeChannel
StopMusic10:
	STZ HandyMusic_Music_Priority,X
	;cli ; Now they can be turned back on.
	DEX
	bpl StopMusic00
	PLX
	rts

;****************************************************************
; HandyMusic_FreeChannel:					*
;	Frees the channel specified by the X register so it may	*
; be available for future notes and instruments. A is trashed.	*
;****************************************************************
HandyMusic_FreeChannel:
	STZ HandyMusic_Channel_Priority,X	; Zero priority, also stops decoding.

	LDY HandyMusic_Redirect_ChOffs,X
	LDA#0
	STA Lynx_Audio_TimerCont,Y
	STA Lynx_Audio_Volume,Y
	STA Lynx_Audio_DirectVol,Y
	DEC
	STA Lynx_Audio_Atten_0,X		; Reset the panning register to $FF
	rts

;****************************************************************
; HandyMusic_Pause:						*
;	Pauses HandyMusic, and mutes all channels. There is 	*
; checking to ensure HandyMusic is not "double paused."		*
;****************************************************************
_HandyMusic_Pause:
	LDA HandyMusic_Enable			; Is HandyMusic already disabled?
	beq return3
	PHX
	PHY
	LDX#3
	;sei					; Disable interrupts only for a bit
	STZ HandyMusic_Enable
backupregs:
	LDY HandyMusic_Redirect_ChOffs,X
	LDA Lynx_Audio_TimerCont,Y
	STA HandyMusic_Pause_TimerBack,X	; Back up Timers, shutting each off
	LDA Lynx_Audio_Volume,Y
	STA HandyMusic_Pause_VolumeBack,X	; Do the same for the volume registers
	LDA#0
	STA Lynx_Audio_Volume,Y
	STA Lynx_Audio_TimerCont,Y
	DEX
	bpl backupregs
	;cli					; Interrupts back on
	PLX
	PLY
return3:
	rts

;****************************************************************
; HandyMusic_UnPause:						*
;	UnPauses HandyMusic, restoring all channels. There is 	*
; checking to ensure HandyMusic is not "double unpaused."	*
;****************************************************************
_HandyMusic_UnPause:
	LDA HandyMusic_Enable			; Is HandyMusic already enabled?
	bne return4
	PHX
	PHY
	LDX#3
	;sei					; Disable IRQs for a bit
restoreregs:
	LDY HandyMusic_Redirect_ChOffs,X
	LDA HandyMusic_Pause_VolumeBack,X
	STA Lynx_Audio_Volume,Y			; Restore Volumes
	LDA HandyMusic_Pause_TimerBack,X
	STA Lynx_Audio_TimerCont,Y		; Restore Timers
	DEX
	bpl restoreregs
	INC HandyMusic_Enable			; HandyMusic and IRQs on
	STZ HandyMusic_SFX_PlayRequest		; Kill any SFX requests during pause
	;cli
	PLX
	PLY
return4:
	rts

;****************************************************************
; HandyMusic_Init:						*
;	Initialize HandyMusic and the audio hardware to a known	*
; state with stereo sound enabled and no channels active.	*
;****************************************************************
_HandyMusic_Init:
	STZ HandyMusic_Enable
	STZ _HandyMusic_Active
	STZ _HandyMusic_BGMPlaying
	STZ HandyMusic_Pause_TimerBack
	STZ HandyMusic_Pause_TimerBack+1
	STZ HandyMusic_Pause_TimerBack+2
	STZ HandyMusic_Pause_TimerBack+3
	STZ HandyMusic_Pause_VolumeBack
	STZ HandyMusic_Pause_VolumeBack+1
	STZ HandyMusic_Pause_VolumeBack+2
	STZ HandyMusic_Pause_VolumeBack+3
	STZ HandyMusic_Channel_NoWriteBack
	STZ HandyMusic_Channel_NoWriteBack+1
	STZ HandyMusic_Channel_NoWriteBack+2
	STZ HandyMusic_Channel_NoWriteBack+3
	STZ HandyMusic_SFX_AddressTableLoLo
	STZ HandyMusic_SFX_AddressTableLoHi
	STZ HandyMusic_SFX_AddressTableHiLo
	STZ HandyMusic_SFX_AddressTableHiHi
	STZ HandyMusic_SFX_AddressTablePriLo
	STZ HandyMusic_SFX_AddressTablePriHi
	STZ HandyMusic_SFX_EnqueueNext
	STZ HandyMusic_SFX_PlayRequest
	STZ HandyMusic_Channel_DecodePointer
	STZ HandyMusic_Channel_DecodePointer+1
	STZ HandyMusic_Channel_Priority
	STZ HandyMusic_Channel_Priority+1
	STZ HandyMusic_Channel_Priority+2
	STZ HandyMusic_Channel_Priority+3
	STZ HandyMusic_Channel_LoopDepth
	STZ HandyMusic_Channel_LoopDepth+1
	STZ HandyMusic_Channel_LoopDepth+2
	STZ HandyMusic_Channel_LoopDepth+3
	STZ HandyMusic_Channel_FinalFreqLo
	STZ HandyMusic_Channel_FinalFreqHi
	STZ HandyMusic_Instrument_AddrTableLoLo
	STZ HandyMusic_Instrument_AddrTableLoHi
	STZ HandyMusic_Instrument_AddrTableHiLo
	STZ HandyMusic_Instrument_AddrTableHiHi
	STZ HandyMusic_Music_DecodePointer
	STZ HandyMusic_Music_DecodePointer+1
	STZ HandyMusic_Music_Priority
	STZ HandyMusic_Music_Priority+1
	STZ HandyMusic_Music_Priority+2
	STZ HandyMusic_Music_Priority+3
	STZ HandyMusic_Music_LoopDepth
	STZ HandyMusic_Music_LoopDepth+1
	STZ HandyMusic_Music_LoopDepth+2
	STZ HandyMusic_Music_LoopDepth+3
	STZ _Sample_Playing
	STZ Sample_PanBackup

;Setup HandyMusic SFX Pointers
	LDA#<__SOUND_EFFECTS__
	STA HandyMusic_SFX_AddressTableLoLo
	LDA#>__SOUND_EFFECTS__
	STA HandyMusic_SFX_AddressTableLoHi
	LDA#<__SOUND_EFFECTS1__
	STA HandyMusic_SFX_AddressTableHiLo
	LDA#>__SOUND_EFFECTS1__
	STA HandyMusic_SFX_AddressTableHiHi
	LDA#<__SOUND_EFFECTS2__
	STA HandyMusic_SFX_AddressTablePriLo
	LDA#>__SOUND_EFFECTS2__
	STA HandyMusic_SFX_AddressTablePriHi
	STZ HandyMusic_Channel_NoWriteBack
	STZ HandyMusic_Channel_NoWriteBack+1
	STZ HandyMusic_Channel_NoWriteBack+2
	STZ HandyMusic_Channel_NoWriteBack+3
; Personal Note : The hardware is initialized
; with all channels off, so we're essentially just going to
; enable stereo and get HandyMusic ready to go.
	stz _HandyMusic_Active
	jsr _HandyMusic_StopAll			; Make sure all channels are open.
	LDA#$FF					; Enable attenuation on all channels,
	STA Lynx_Audio_Panning			; I'm guessing the write is just lost on the Lynx 1 (ed- I checked, open bus)
	STZ Lynx_Audio_Stereo			; Enable output in both L&R
	STA HandyMusic_Enable			; Now enable HandyMusic.
	rts



;****************************************************************
;			  General Vars				*
;****************************************************************
.segment "HM_BSS"
;***************
; For Channels *
;***************
HandyMusic_Channel_BaseFreqLo:	.res 4
HandyMusic_Channel_BaseFreqHi:	.res 4
HandyMusic_Channel_BaseFreqDec:	.res 4
; The base frequency for each channel stored in 16.8 precision, 
; this is set to zero when playing a sound effect, but is used
; for the note frequency on instruments.
; Each frame this value is adjusted by: BaseFrequency+=BasePitchAdjust

HandyMusic_Channel_BasePitAdjLo:	.res 4
HandyMusic_Channel_BasePitAdjHi:	.res 4
HandyMusic_Channel_BasePitAdjDec:	.res 4
; The adjustment frequency for each channel stored in 16.8 precision,
; this is unused in SFX, but is used to alter the current note
; frequency in instruments (i.e. Pitch Bends).

HandyMusic_Channel_FreqOffsetLo:	.res 4
HandyMusic_Channel_FreqOffsetHi:	.res 4
HandyMusic_Channel_FreqOffsetDec:	.res 4
; The offset from the base frequency stored in 16.8 precision,
; this is used in both notes and sound effects to calculate the
; final frequency, which is performed each frame as follows:
; Output Frequency = (FrequencyOffset+=OffsetPitchAdjust)+(BaseFrequency+=BasePitchAdjust)

HandyMusic_Channel_OffsetPitAdjLo:	.res 4
HandyMusic_Channel_OffsetPitAdjHi:	.res 4
HandyMusic_Channel_OffsetPitAdjDec:	.res 4
; The adjustment frequency for each offset frequency stored in 16.8 precision,
; this is used in both SFX and notes to generate additional
; frequency changes such as pitch slides or vibrato.

HandyMusic_Channel_LastFreqLo:	.res 4
HandyMusic_Channel_LastFreqHi:	.res 4
; The previous frequency written to the channel, used to determine 
; if a channel should have its frequency register
; updated during its processing time.

HandyMusic_Channel_ForceUpd:	.res 4
; Will force an update to the channel's timer at the end of
; processing if nonzero.

HandyMusic_Channel_Volume:	.res 4
HandyMusic_Channel_VolumeDec:	.res 4
; The volume for each channel stored in 8.8 precision,
; each frame this value is adjusted as: Volume+=VolumeAdjust

HandyMusic_Channel_VolumeAdjust:	.res 4
HandyMusic_Channel_VolumeAdjustDec:	.res 4
; The volume adjustment for each channel stored in 8.8 precision,
; this is used by both instruments and sound effects to generate
; volume envelopes.

HandyMusic_Channel_Panning:	.res 4
; The panning of the current channel, this is only usable by
; notes. Sound effects have a constant panning of $FF (center),
; and ignore this value. Format is LLLLRRRR.

HandyMusic_Channel_DecodePointerLo:	.res 4
HandyMusic_Channel_DecodePointerHi:	.res 4
; The current instrumet/sfx script decode pointer.

HandyMusic_Channel_DecodeDelay:	.res 4
; Delays the decoding of the current channel's instrument/sfx script.
; All volume/frequency envelope processing is still continued, however.

HandyMusic_Channel_NoteOffPLo:	.res 4
HandyMusic_Channel_NoteOffPHi:	.res 4
; Specifically for instruments, contains the pointer to the "note off"
; portion of a script.  Unused by sound effects.

HandyMusic_Channel_LoopAddrLo:	.res 16
HandyMusic_Channel_LoopAddrHi:	.res 16
HandyMusic_Channel_LoopCount:	.res 16
.segment "HM_DATA"
HandyMusic_Channel_LoopAddrDepth:	.byte 0,4,8,12
.segment "HM_BSS"
; The address buffers used for script looping
;********************
; For Music Scripts *
;********************
HandyMusic_Music_DecodePointerLo:	.res 4
HandyMusic_Music_DecodePointerHi:	.res 4
; The current music script decode pointer.

HandyMusic_Music_DecodeDelayLo:	.res 4
HandyMusic_Music_DecodeDelayHi:	.res 4
; Delays the decoding of the next music script action by a
; 16 bit value.

HandyMusic_Music_LoopAddrLo:	.res 16
HandyMusic_Music_LoopAddrHi:	.res 16
HandyMusic_Music_LoopCount:	.res 16
; The address buffers used for script looping

HandyMusic_Music_BasePitAdjLo:	.res 4
HandyMusic_Music_BasePitAdjHi:	.res 4
HandyMusic_Music_BasePitAdjDec:	.res 4
; Mirrors of the values in the channel region, only copied in
; when a note is played.

HandyMusic_Music_LastInstrument:	.res 4
; The last instrument used by the music script.
;*********
; Extras *
;*********
HandyMusic_Disable_Samples:	.res 1
; Sample playback in both music and user requests will be
; dropped when this is nonzero, preferably should be set
; when loading from the cartridge.
.segment "HM_DATA"
;********************
; Redirection Table *
;********************
HandyMusic_Redirect_ChOffs:		.byte 0,8,16,24
; These set of values are used to redirect the audio hardware
; addreses to an order which is "much more pleasing" to the programmer.
; This way they may be quickly accessed through ,X or ,Y addressing.
;HandyMusic_Note .byte "No more room!"
;************************
; HandyMusic Space Left *
;************************
;SDLeft set $A6AF-*
;echo "HandyMusic Memory left: %HSDLeft"
