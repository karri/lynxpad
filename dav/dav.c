#include <lynx.h>
#include <tgi.h>
#include "joystick.h"
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>

extern unsigned char checkInput ();
extern void nextcursor ();
static char *dummy = "Dummy";

enum {
    EMPTY,
    LIBSOUND,
    SOUND,
    LIBCLIP,
    CLIP,
    ACTIVE
};

extern void setbtn(signed char row, signed char col, unsigned char color);

extern void drawpad();

static unsigned char endgame() {
    return 1;
}

static clock_t now;
static clock_t blinktime;
static unsigned char beatsPerBar;
static unsigned char prevbeat;
static unsigned char beat;
static unsigned char prevqbeat;
static unsigned char prevqbeatrow;
static unsigned char qbeat;
static unsigned char chanOn[4];
static unsigned char copyfrom[4];
static unsigned char copysnd[4];
static unsigned char chan1[8];
static unsigned char chan2[8];
static unsigned char chan3[8];
static unsigned char chan4[8];
static unsigned char chan1playing;
static unsigned char chan2playing;
static unsigned char chan3playing;
static unsigned char chan4playing;
static unsigned char cliplibset;
static unsigned char cliplib[8];
static unsigned char cliplibedit;
static unsigned char sndlibset;
static unsigned char sndlib[8];
static unsigned char pressed[8];
static unsigned char pianosnd[4];
static unsigned char pianoC;
static unsigned char tick;
static unsigned int sixteenthtime;
static unsigned int trioletime;
static unsigned char rhythmmode;
static unsigned int tap;
static unsigned char plus8;
static unsigned char plus16;
static unsigned char showfrombeat;
static unsigned char patternlen;
static unsigned char currcliplen;
static unsigned char currclippattern;

static void settick(unsigned char ticks)
{
    tick = ticks;
    sixteenthtime = tick * 3;
    trioletime = tick * 4;
}

static void setbeat(int quartertime) {
    settick(quartertime / 12);
}

static void init() {
    now = clock();
    blinktime = now;
    beatsPerBar = 4;
    prevbeat = 0;
    beat = 0;
    prevqbeat = 0;
    prevqbeatrow = 0;
    qbeat = 0;
    chanOn[0] = 1;
    chanOn[1] = 1;
    chanOn[2] = 1;
    chanOn[3] = 1;
    settick(4);
    rhythmmode = 1;
    currcliplen = 7;
    showfrombeat = 0;
    pianoC = 0;
    pianosnd[0] = 255;
    plus8 = 0;
    plus16 = 0;
    currclippattern = 4;
}

static void showbeat()
{
    setbtn(prevbeat & 7, 8, EMPTY);
    setbtn(beat & 7, 8, LIBCLIP);
    prevbeat = beat;
    if (rhythmmode) {
        if ((prevqbeatrow >= showfrombeat) &&
            (prevqbeatrow < showfrombeat + 8)) {
            // check currclip
            setbtn(prevqbeatrow & 7,prevqbeat,EMPTY);
            prevqbeatrow = beat;
            prevqbeat = qbeat;
            if ((beat >= showfrombeat) &&
                (beat < showfrombeat + 8)) {
                setbtn(beat & 7,qbeat,CLIP);
            }
        }
    }
    if (qbeat == 0) {
        setbtn(-1,6,CLIP);
    } else {
        setbtn(-1,6,EMPTY);
    }
}

static void nextbeat()
{
    int elementtime;
    unsigned char elementnr;

    elementtime = sixteenthtime;
    elementnr = 4;
    // if currclippattern
    if (clock() - now > elementtime) {
        now = now + elementtime;
        showbeat();
        qbeat = qbeat + 1;
        if (qbeat >= elementnr) {
            qbeat = 0;
            beat = beat + 1;
            if (beat > currcliplen) {
                beat = 0;
            }
        }
    }
}

static void changePianoC(signed char incr) {
    pianoC = pianoC + incr;
}

static signed char pianowrap(signed char row) {
    row = row + pianoC;
    while (row < 0) {
        row = row + 7;
    }
    while (row > 6) {
        row = row - 7;
    }
    return row;
}

static void drawpiano() {
    unsigned char y;

    for (y = 0; y < 8; y++) {
        setbtn(y,7,CLIP);
    }
    setbtn(pianowrap(6), 6, LIBCLIP);
    if (pianosnd[0] != 0) {
        setbtn(pianowrap(4), 6, LIBSOUND);
    } else {
        setbtn(pianowrap(4), 6, LIBCLIP);
    }
    setbtn(pianowrap(1), 6, EMPTY);
    setbtn(pianowrap(2), 6, EMPTY);
    setbtn(pianowrap(3), 6, EMPTY);
    setbtn(pianowrap(5), 6, EMPTY);
    setbtn(pianowrap(6), 6, EMPTY);
    setbtn(0, 6, SOUND);
    setbtn(7, 6, SOUND);
}

static void refresh(unsigned char col) {
    switch (col) {
    default:
        // drawchans();
        break;
    case 4:
        // drawcliplib();
        break;
    case 5:
        // drawsndlib();
        break;
    case 6:
    case 7:
        drawpiano();
        break;
    }
}

static void drawchanctrls() {
    unsigned char chan;

    for (chan = 0; chan < 4; chan++) {
        if (rhythmmode) {
            switch (chan) {
            case 0:
                if (plus8) {
                    setbtn(-1, 0, CLIP);
                } else {
                    setbtn(-1, 0, EMPTY);
                }
                break;
            case 1:
                if (plus16) {
                    setbtn(-1, 1, CLIP);
                } else {
                    setbtn(-1, 1, EMPTY);
                }
                break;
            default:
                if (chan + 1 == currclippattern) {
                    setbtn(-1, chan, CLIP);
                } else {
                    setbtn(-1, chan, EMPTY);
                }
                break;
            }
        } else {
            if (chanOn[chan]) {
                setbtn(-1, chan, ACTIVE);
            } else {
                setbtn(-1, chan, EMPTY);
            }
        }
    }
}

static void saveclip() {
}

static void togglemutectrl(unsigned char chan) {
    chanOn[chan] = 1 - chanOn[chan];
    drawchanctrls();
}

static void notelen(unsigned char val) {
    switch (val) {
    case 2:
        currclippattern = 3;
        saveclip();
        break;
    case 3:
        currclippattern = 4;
        saveclip();
        break;
    default:
        showfrombeat = 0;
        if (val == 0) {
            plus8 = 1 - plus8;
        }
        if (val == 1) {
            plus16 = 1 - plus16;
        }
        if (plus8) {
            showfrombeat = 8;
        }
        if (plus16) {
            showfrombeat += 16;
        }
        break;
    }
    //drawfrombeat();
    drawchanctrls();
}

static void drawrhythmmode() {
    if (rhythmmode) {
        setbtn(-1, 7, SOUND);
    } else {
        setbtn(-1, 7, ACTIVE);
    }
}

static void drawcliplib() {
}

static void drawfrombeat() {
}

static void drawsndlib() {
}

static char *maptone() {
    return "C";
}

static unsigned char insertsnd(unsigned char row,
    unsigned char col) {
    return 0;
}

static unsigned char insertclip(unsigned char row,
    unsigned char col) {
    return 0;
}

static void drawchan1() {
    unsigned char y;

    for (y = 0; y < 8; y++) {
        if (chan1[y] == 0) {
            setbtn(y, 0, EMPTY);
        } else {
            if (chan1playing == y) {
                setbtn(y, 0, ACTIVE);
            } else {
                setbtn(y, 0, CLIP);
            }
        }
    }
}

static void drawchan2() {
    unsigned char y;

    for (y = 0; y < 8; y++) {
        if (chan2[y] == 0) {
            setbtn(y, 1, EMPTY);
        } else {
            if (chan2playing == y) {
                setbtn(y, 1, ACTIVE);
            } else {
                setbtn(y, 1, CLIP);
            }
        }
    }
}

static void drawchan3() {
    unsigned char y;

    for (y = 0; y < 8; y++) {
        if (chan3[y] == 0) {
            setbtn(y, 2, EMPTY);
        } else {
            if (chan3playing == y) {
                setbtn(y, 2, ACTIVE);
            } else {
                setbtn(y, 2, CLIP);
            }
        }
    }
}

static void drawchan4() {
    unsigned char y;

    for (y = 0; y < 8; y++) {
        if (chan4[y] == 0) {
            setbtn(y, 3, EMPTY);
        } else {
            if (chan4playing == y) {
                setbtn(y, 3, ACTIVE);
            } else {
                setbtn(y, 3, CLIP);
            }
        }
    }
}

static void drawchans() {
    drawchan1();
    drawchan2();
    drawchan3();
    drawchan4();
}

static unsigned char isEmpty(unsigned char val) {
    return 1;
}

static void ledscolor(unsigned char row,
    unsigned char col,
    unsigned char val) {
    switch (val) {
    case 1:
        setbtn(row, col, LIBSOUND);
        return;
    case 2:
        setbtn(row, col, SOUND);
        return;
    case 3:
        setbtn(row, col, LIBCLIP);
        return;
    case 4:
        setbtn(row, col, CLIP);
        return;
    case 5:
        setbtn(row, col, ACTIVE);
        return;
    default:
        setbtn(row, col, EMPTY);
        return;
    }
}

static void nextcliplibset() {
    cliplibset += 1;
    if (cliplibset > 5) cliplibset = 0;
    ledscolor(-1, 4, cliplibset);
}

static void nextsndlibset() {
    sndlibset += 1;
    if (sndlibset > 5) sndlibset = 0;
    ledscolor(-1, 5, sndlibset);
}

static void setcurrcliplen(unsigned char val) {
    currcliplen = val;
    saveclip();
}

static unsigned char getcopydata(unsigned char row,
    unsigned char col) {
    return 0;
}

static unsigned char copyallowed(unsigned char row,
    unsigned char col) {
    return 0;
}

static unsigned char pasteallowed(unsigned char row,
    unsigned char col) {
    return 0;
}

static void releasebutton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressTopRowButton(signed char row,
    unsigned char col, unsigned char up) {

    switch (col) {
    default:
        break;
    case 7:
        rhythmmode = 1 - rhythmmode;
        drawrhythmmode();
        break;
    }
}

static void pressRightmostColumnButton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressPianoSharpColumnButton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressPianoColumnButton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressCliplibColumnButton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressSndlibColumnButton(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void pressbutton(signed char row,
    unsigned char col, unsigned char up) {

    // append to pressed row, col, time

    if (row == -1) {
        pressTopRowButton(row, col, up);
    }
}

static void processui(unsigned char row,
    unsigned char col, unsigned char up) {
}

static void readclips() {
}

static void readsnds() {
}

static void loadchans() {
}

static void startup() {
    drawrhythmmode();
    drawchanctrls();
    drawpiano();
    readclips();
    readsnds();
    drawcliplib();
    drawsndlib();
    loadchans();
    drawchans();
    nextcursor();
    nextcursor();
    nextcursor();
    nextcursor();
    nextcursor();
}

static void eraseclip() {
}

static void loadclip(unsigned char nr) {
}

static void savechans() {
}

extern void movecursor(signed char row, signed char col);

static clock_t kbd_delay = 0;

static unsigned char btnAup = 1;
static unsigned char btnBup = 1;
static unsigned char btn1up = 1;
static unsigned char btn2up = 1;
extern signed char rowA, colA;
extern signed char rowB, colB;
extern signed char row1, col1;
extern signed char row2, col2;

static void periodic() {
    unsigned char row;
    unsigned char col;
    unsigned char up;
    unsigned char joy;
    joy = checkInput();
    if (clock() > kbd_delay) {
        if (JOY_BTN_LEFT(joy)) {
            movecursor(0,-1);
            kbd_delay = clock() + 10;
        }
        if (JOY_BTN_RIGHT(joy)) {
            movecursor(0,1);
            kbd_delay = clock() + 10;
        }
        if (JOY_BTN_DOWN(joy)) {
            movecursor(1,0);
            kbd_delay = clock() + 10;
        }
        if (JOY_BTN_UP(joy)) {
            movecursor(-1,0);
            kbd_delay = clock() + 10;
        }
        if (JOY_BTN_FIRE(joy)) {
            if (btnAup) {
                btnAup = 0;
                pressbutton(rowA, colA, btnAup);
            }
        } else {
            if (btnAup == 0) {
                btnAup = 1;
                releasebutton(rowA, colA, btnAup);
            }
        }
        if (JOY_BTN_FIRE2(joy)) {
            if (btnBup) {
                btnBup = 0;
                pressbutton(rowB, colB, btnBup);
            }
        } else {
            if (btnBup == 0) {
                btnBup = 1;
                releasebutton(rowB, colB, btnBup);
            }
        }
        if (JOY_BTN_OPT1(joy)) {
            if (btn1up) {
                btn1up = 0;
                pressbutton(row1, col1, btn1up);
            }
        } else {
            if (btn1up == 0) {
                btn1up = 1;
                releasebutton(row1, col1, btn1up);
            }
        }
        if (JOY_BTN_OPT2(joy)) {
            if (btn2up) {
                btn2up = 0;
                pressbutton(row2, col2, btn2up);
            }
        } else {
            if (btn2up == 0) {
                btn2up = 1;
                releasebutton(row2, col2, btn2up);
            }
        }
    }
    nextbeat();
    processui(row, col, up);
}
  

void dav(void) 
{
    init();
    startup();
    while (endgame()) {
        periodic();
        if (tgi_busy() == 0) {
            tgi_setcolor(COLOR_YELLOW);
            tgi_bar(0,0,159,101);
            drawpad();
            tgi_updatedisplay();
        }
    }
    while (checkInput())
        ;
} 

