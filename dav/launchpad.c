#include <lynx.h>
#include <tgi.h>

extern unsigned char emptybtn[];
extern unsigned char libsndbtn[];
extern unsigned char sndbtn[];
extern unsigned char libclipbtn[];
extern unsigned char clipbtn[];
extern unsigned char activebtn[];
extern unsigned char cA[];
extern unsigned char cB[];
extern unsigned char c1[];
extern unsigned char c2[];

unsigned char cursor = 3;
signed char rowA = 0;
signed char colA = 8;
signed char rowB = 1;
signed char colB = 8;
signed char row1 = 2;
signed char col1 = 8;
signed char row2 = 3;
signed char col2 = 8;

static SCB_REHV_PAL Spr2 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    c2,
    2 + 8*17 + 8, 2 + 4 * 11 + 5,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr1 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr2,
    c1,
    2 + 8*17 + 8, 2 + 3 * 11 + 5,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL SprB = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr1,
    cB,
    2 + 8*17 + 8, 2 + 2 * 11 + 5,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL SprA = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&SprB,
    cA,
    2 + 8*17 + 8, 2 + 1 * 11 + 5,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr88 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&SprA,
    emptybtn,
    2 + 8*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr87 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr88,
    emptybtn,
    2 + 7*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr86 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr87,
    emptybtn,
    2 + 6*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr85 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr86,
    emptybtn,
    2 + 5*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr84 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr85,
    emptybtn,
    2 + 4*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr83 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr84,
    emptybtn,
    2 + 3*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr82 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr83,
    emptybtn,
    2 + 2*17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr81 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr82,
    emptybtn,
    2 + 17, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr80 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr81,
    emptybtn,
    2, 2 + 8 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr78 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr80,
    emptybtn,
    2 + 8*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr77 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr78,
    emptybtn,
    2 + 7*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr76 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr77,
    emptybtn,
    2 + 6*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr75 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr76,
    emptybtn,
    2 + 5*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr74 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr75,
    emptybtn,
    2 + 4*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr73 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr74,
    emptybtn,
    2 + 3*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr72 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr73,
    emptybtn,
    2 + 2*17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr71 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr72,
    emptybtn,
    2 + 17, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr70 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr71,
    emptybtn,
    2, 2 + 7 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr68 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr70,
    emptybtn,
    2 + 8*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr67 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr68,
    emptybtn,
    2 + 7*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr66 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr67,
    emptybtn,
    2 + 6*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr65 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr66,
    emptybtn,
    2 + 5*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr64 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr65,
    emptybtn,
    2 + 4*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr63 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr64,
    emptybtn,
    2 + 3*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr62 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr63,
    emptybtn,
    2 + 2*17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr61 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr62,
    emptybtn,
    2 + 17, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr60 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr61,
    emptybtn,
    2, 2 + 6 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr58 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr60,
    emptybtn,
    2 + 8*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr57 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr58,
    emptybtn,
    2 + 7*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr56 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr57,
    emptybtn,
    2 + 6*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr55 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr56,
    emptybtn,
    2 + 5*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr54 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr55,
    emptybtn,
    2 + 4*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr53 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr54,
    emptybtn,
    2 + 3*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr52 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr53,
    emptybtn,
    2 + 2*17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr51 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr52,
    emptybtn,
    2 + 17, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr50 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr51,
    emptybtn,
    2, 2 + 5 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr48 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr50,
    emptybtn,
    2 + 8*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr47 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr48,
    emptybtn,
    2 + 7*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr46 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr47,
    emptybtn,
    2 + 6*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr45 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr46,
    emptybtn,
    2 + 5*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr44 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr45,
    emptybtn,
    2 + 4*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr43 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr44,
    emptybtn,
    2 + 3*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr42 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr43,
    emptybtn,
    2 + 2*17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr41 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr42,
    emptybtn,
    2 + 17, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr40 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr41,
    emptybtn,
    2, 2 + 4 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr38 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr40,
    emptybtn,
    2 + 8*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr37 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr38,
    emptybtn,
    2 + 7*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr36 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr37,
    emptybtn,
    2 + 6*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr35 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr36,
    emptybtn,
    2 + 5*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr34 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr35,
    emptybtn,
    2 + 4*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr33 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr34,
    emptybtn,
    2 + 3*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr32 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr33,
    emptybtn,
    2 + 2*17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr31 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr32,
    emptybtn,
    2 + 17, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr30 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr31,
    emptybtn,
    2, 2 + 3 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr28 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr30,
    emptybtn,
    2 + 8*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr27 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr28,
    emptybtn,
    2 + 7*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr26 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr27,
    emptybtn,
    2 + 6*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr25 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr26,
    emptybtn,
    2 + 5*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr24 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr25,
    emptybtn,
    2 + 4*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr23 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr24,
    emptybtn,
    2 + 3*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr22 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr23,
    emptybtn,
    2 + 2*17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr21 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr22,
    emptybtn,
    2 + 17, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr20 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr21,
    emptybtn,
    2, 2 + 2 * 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr18 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr20,
    emptybtn,
    2 + 8*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr17 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr18,
    emptybtn,
    2 + 7*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr16 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr17,
    emptybtn,
    2 + 6*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr15 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr16,
    emptybtn,
    2 + 5*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr14 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr15,
    emptybtn,
    2 + 4*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr13 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr14,
    emptybtn,
    2 + 3*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr12 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr13,
    emptybtn,
    2 + 2*17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr11 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr12,
    emptybtn,
    2 + 17, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr10 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr11,
    emptybtn,
    2, 2 + 11,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr08 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr10,
    emptybtn,
    2 + 8*17, 2,
    0x100, 0x100,
    {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}
};

static SCB_REHV_PAL Spr07 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr08,
    emptybtn,
    2 + 7*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr06 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr07,
    emptybtn,
    2 + 6*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr05 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr06,
    emptybtn,
    2 + 5*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr04 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr05,
    emptybtn,
    2 + 4*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr03 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr04,
    emptybtn,
    2 + 3*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr02 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr03,
    emptybtn,
    2 + 2*17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr01 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr02,
    emptybtn,
    2 + 17, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL Spr00 = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    (char *)&Spr01,
    emptybtn,
    2, 2,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

void setbtn(signed char row, signed char col, unsigned char color)
{
    unsigned char index;
    unsigned char **data;

    index = (row + 1) * 9 + col;
    switch (index) {
    case 0:
        data = &Spr00.data;
        break;
    case 1:
        data = &Spr01.data;
        break;
    case 2:
        data = &Spr02.data;
        break;
    case 3:
        data = &Spr03.data;
        break;
    case 4:
        data = &Spr04.data;
        break;
    case 5:
        data = &Spr05.data;
        break;
    case 6:
        data = &Spr06.data;
        break;
    case 7:
        data = &Spr07.data;
        break;
    case 9:
        data = &Spr10.data;
        break;
    case 10:
        data = &Spr11.data;
        break;
    case 11:
        data = &Spr12.data;
        break;
    case 12:
        data = &Spr13.data;
        break;
    case 13:
        data = &Spr14.data;
        break;
    case 14:
        data = &Spr15.data;
        break;
    case 15:
        data = &Spr16.data;
        break;
    case 16:
        data = &Spr17.data;
        break;
    case 17:
        data = &Spr18.data;
        break;
    case 18:
        data = &Spr20.data;
        break;
    case 19:
        data = &Spr21.data;
        break;
    case 20:
        data = &Spr22.data;
        break;
    case 21:
        data = &Spr23.data;
        break;
    case 22:
        data = &Spr24.data;
        break;
    case 23:
        data = &Spr25.data;
        break;
    case 24:
        data = &Spr26.data;
        break;
    case 25:
        data = &Spr27.data;
        break;
    case 26:
        data = &Spr28.data;
        break;
    case 27:
        data = &Spr30.data;
        break;
    case 28:
        data = &Spr31.data;
        break;
    case 29:
        data = &Spr32.data;
        break;
    case 30:
        data = &Spr33.data;
        break;
    case 31:
        data = &Spr34.data;
        break;
    case 32:
        data = &Spr35.data;
        break;
    case 33:
        data = &Spr36.data;
        break;
    case 34:
        data = &Spr37.data;
        break;
    case 35:
        data = &Spr38.data;
        break;
    case 36:
        data = &Spr40.data;
        break;
    case 37:
        data = &Spr41.data;
        break;
    case 38:
        data = &Spr42.data;
        break;
    case 39:
        data = &Spr43.data;
        break;
    case 40:
        data = &Spr44.data;
        break;
    case 41:
        data = &Spr45.data;
        break;
    case 42:
        data = &Spr46.data;
        break;
    case 43:
        data = &Spr47.data;
        break;
    case 44:
        data = &Spr48.data;
        break;
    case 45:
        data = &Spr50.data;
        break;
    case 46:
        data = &Spr51.data;
        break;
    case 47:
        data = &Spr52.data;
        break;
    case 48:
        data = &Spr53.data;
        break;
    case 49:
        data = &Spr54.data;
        break;
    case 50:
        data = &Spr55.data;
        break;
    case 51:
        data = &Spr56.data;
        break;
    case 52:
        data = &Spr57.data;
        break;
    case 53:
        data = &Spr58.data;
        break;
    case 54:
        data = &Spr60.data;
        break;
    case 55:
        data = &Spr61.data;
        break;
    case 56:
        data = &Spr62.data;
        break;
    case 57:
        data = &Spr63.data;
        break;
    case 58:
        data = &Spr64.data;
        break;
    case 59:
        data = &Spr65.data;
        break;
    case 60:
        data = &Spr66.data;
        break;
    case 61:
        data = &Spr67.data;
        break;
    case 62:
        data = &Spr68.data;
        break;
    case 63:
        data = &Spr70.data;
        break;
    case 64:
        data = &Spr71.data;
        break;
    case 65:
        data = &Spr72.data;
        break;
    case 66:
        data = &Spr73.data;
        break;
    case 67:
        data = &Spr74.data;
        break;
    case 68:
        data = &Spr75.data;
        break;
    case 69:
        data = &Spr76.data;
        break;
    case 70:
        data = &Spr77.data;
        break;
    case 71:
        data = &Spr78.data;
        break;
    case 72:
        data = &Spr80.data;
        break;
    case 73:
        data = &Spr81.data;
        break;
    case 74:
        data = &Spr82.data;
        break;
    case 75:
        data = &Spr83.data;
        break;
    case 76:
        data = &Spr84.data;
        break;
    case 77:
        data = &Spr85.data;
        break;
    case 78:
        data = &Spr86.data;
        break;
    case 79:
        data = &Spr87.data;
        break;
    case 80:
        data = &Spr88.data;
        break;
    }
    switch (color) {
    case 0:
        *data = emptybtn;
        break;
    case 1:
        *data = libsndbtn;
        break;
    case 2:
        *data = sndbtn;
        break;
    case 3:
        *data = libclipbtn;
        break;
    case 4:
        *data = clipbtn;
        break;
    case 5:
        *data = activebtn;
        break;
    }
}

void drawpad() {
    tgi_sprite(&Spr00);
}

static void setCursorColour(unsigned char active) {
    switch (cursor) {
    case 0:
        SprA.penpal[7] = active ? 0xed : 0xef;
        return;
    case 1:
        SprB.penpal[7] = active ? 0xed : 0xef;
        return;
    case 2:
        Spr1.penpal[7] = active ? 0xed : 0xef;
        return;
    case 3:
        Spr2.penpal[7] = active ? 0xed : 0xef;
        return;
    }
}

void nextcursor() {
    setCursorColour(0);
    cursor = (cursor + 1) & 3;
    setCursorColour(1);
}

static void cursorhlp(
    signed char *rowC,
    signed char *colC,
    int *hpos,
    int *vpos,
    signed char row,
    signed char col) {
    *rowC += row;
    if (*rowC < -1) *rowC = 7;
    if (*rowC > 7) *rowC = -1;
    *vpos = 2 + (*rowC + 1) * 11 + 5;
    *colC += col;
    if (*colC < 0) *colC = 8;
    if (*colC > 8) *colC = 0;
    *hpos = 2 + (*colC) * 17 + 8;
}

void movecursor(signed char row, signed char col) {
    switch (cursor) {
    case 0:
        cursorhlp(&rowA, &colA, &(SprA.hpos), &(SprA.vpos), row, col);
        break;
    case 1:
        cursorhlp(&rowB, &colB, &(SprB.hpos), &(SprB.vpos), row, col);
        break;
    case 2:
        cursorhlp(&row1, &col1, &(Spr1.hpos), &(Spr1.vpos), row, col);
        break;
    case 3:
        cursorhlp(&row2, &col2, &(Spr2.hpos), &(Spr2.vpos), row, col);
        break;
    }
}

