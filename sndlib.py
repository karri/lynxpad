'''
    The sndlib instruments are grouped into 6
    categories

    8 percussion instruments
    8 string instruments
    8 wind instruments
    8 solo instruments
    8 sound effects
    8 misc
'''

class sndlib:
    def __init__(self):
        self.names = []
        ''' Percussion sounds '''
        self.names.append('Kick')
        self.names.append('Bass_Drum')
        self.names.append('Clap')
        self.names.append('Cymbal')
        self.names.append('Tish')
        self.names.append('UpTish')
        self.names.append('SnareShort')
        self.names.append('SnareLong')
        ''' String instruments '''
        self.names.append('Pulse25')
        self.names.append('Pulse25_SlowDecay')
        self.names.append('Pulse33_Quiet')
        self.names.append('Electar')
        self.names.append('PulseWap')
        self.names.append('WaWap')
        self.names.append('DoWap')
        self.names.append(None)
        ''' Wind instruments '''
        self.names.append('WarblyTriangle')
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        ''' Solo instruments '''
        self.names.append('Pulse14')
        self.names.append('Pulse20')
        self.names.append('Pulse33')
        self.names.append('Pulse50')
        self.names.append('SharpySine')
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        ''' Sound effects '''
        self.names.append('Capcom_Trislide')
        self.names.append('TrianglePowPow')
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        ''' Misc sounds '''
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)
        self.names.append(None)

    def sndnames(self):
        return self.names

if __name__ == "__main__":
    p = sndlib()
    nr = 0
    for i in p.sndnames():
        print(nr, i)
        nr = nr + 1

