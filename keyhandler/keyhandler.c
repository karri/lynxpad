#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>
#include <string.h>

extern void nextcursor();
static char *dummystr="Dummy";
extern void HandyMusic_Init ();
extern void HandyMusic_PlayMusic ();
extern void HandyMusic_Pause ();
extern void HandyMusic_UnPause ();
extern void HandyMusic_Main ();
extern void HandyMusic_StopAll();
extern void HandyMusic_StopMusic ();
extern void __fastcall__ HandyMusic_LoadPlayBGM(unsigned char filenr);
extern unsigned char Sample_Playing;
extern unsigned char currentmusic;
#pragma zpsym("Sample_Playing");
#define PickSFX 0
#define PlingSFX 1
#define BombSFX 2
#define MachinegunSFX 3
#define ChopperSFX 4
#define ChopperSFXpriority 236
#define AlertSFX 5
#define ExplosionSFX 6
#define StepSFX 7
#define LocknloadSFX 8
extern void __fastcall__ HandyMusic_PlaySFX(unsigned char sfx);

extern unsigned char bgmusic;
extern unsigned char halted;
extern unsigned char reset;
extern unsigned char invulnerable;
extern clock_t completedmissions[];
extern unsigned char check_eeprom;
extern unsigned char startcredits;
extern unsigned char ingame;

static unsigned char WaitForRelease = 0;
static unsigned char WaitForKeyRelease = 0;
static unsigned char backupPal[32];
static unsigned char grayPal[32];

unsigned char
checkInput (void)
{
  unsigned int col;
  unsigned char i;
  const unsigned char *pal;

  reset = 0;

  if (!reset)
    do
      {
	    if (kbhit ())
	      {
                //if (WaitForKeyRelease == 0)
                  {
                    //WaitForKeyRelease = 1;
	            switch (cgetc ())
	              {
	              case 'F':
		        tgi_flip ();
		        break;
	              case 'P':
                      nextcursor();
		        break;
	              case 'R':
		        reset = 1;
		        break;

	              case '1':
		        break;
	              case '2':
		        break;

	              case '3':	// used to clear saves on eeprom
                        check_eeprom = 3;
			reset = 1;
		        break;

	              case '?':
		        break;

	              default:
		        break;
	              }
	          }
	      }
            else
              {
                WaitForKeyRelease = 0;
              }
      }
    while (halted && !reset);

  return joy_read (JOY_1);
}
