all:
	"$(MAKE)" -C resident;
	"$(MAKE)" -C keyhandler;
	"$(MAKE)" -C loader;
	"$(MAKE)" -C intro;
	"$(MAKE)" -C dav;
	"$(MAKE)" -C music;
	"$(MAKE)" -C sfx;
	"$(MAKE)" -C samples;
	"$(MAKE)" -C cart clean;
	"$(MAKE)" -C cart;

clean:
	"$(MAKE)" -C resident clean;
	"$(MAKE)" -C keyhandler clean;
	"$(MAKE)" -C loader clean;
	"$(MAKE)" -C intro clean;
	"$(MAKE)" -C dav clean;
	"$(MAKE)" -C music clean;
	"$(MAKE)" -C sfx clean;
	"$(MAKE)" -C samples clean;
	"$(MAKE)" -C cart clean;

