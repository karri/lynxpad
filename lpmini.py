'''
    Launchpad Mini Mk2

    Karri Kaksonen, 2020
'''
import select
import os

class lpmini:

    '''
        Open launchpad
    '''
    def __init__(self):
        self.dev = '/dev/dmmidi1'
        if not os.path.exists(self.dev):
            self.dev = '/dev/dmmidi2'
        if os.path.exists(self.dev):
            self.f = open(self.dev, 'rb')
            self.g = open(self.dev, 'wb', 0)
        else:
            self.f = None
            self.g = None
        self.pressed = []

    '''
        Reset launchpad
    '''
    def reset(self):
        s = bytearray()
        s.append(176)
        s.append(0)
        s.append(0)
        if not self.g is None:
            self.g.write(s)

    '''
        Grid mapping
        1 = default X-Y
        2 = drum rack (midi)
    '''
    def gridmap(self, mode):
        s = bytearray()
        s.append(176)
        s.append(0)
        s.append(mode)
        if not self.g is None:
            self.g.write(s)

    '''
        Control double buffering
    '''
    def ctrlbuf(self, display2draw, flash, drawbuf, displaybuf):
        s = bytearray()
        s.append(176)
        s.append(0)
        val = 32
        if display2draw:
            val = val + 16
        if flash:
            val = val + 8
        if drawbuf == 1:
            val = val + 4
        if displaybuf == 1:
            val = val + 1
        s.append(val)
        if not self.g is None:
            self.g.write(s)

    '''
        All LEDs on
        125 = low
        126 = medium
        127 = high
    '''
    def allOn(self, brightness):
        s = bytearray()
        s.append(176)
        s.append(0)
        s.append(brightness)
        if not self.g is None:
            self.g.write(s)

    '''
        Duty cycle = numerator (1..16) / denominator (3..18)
        default 1 / 5
    '''
    def dutyCycle(self, numerator, denominator):
        s = bytearray()
        s.append(176)
        if numerator < 9:
            s.append(30)
            s.append(16 * (numerator - 1) + (denominator - 3))
        else:
            s.append(31)
            s.append(16 * (numerator - 9) + (denominator - 3))
        if not self.g is None:
            self.g.write(s)

    '''
        Rapid LED update
        Send 8 x 8 pad row at a time
        Followed by the ABCD.. column
        Followed by top row 1234...
    '''
    def rapidLed(self, colour1, colour2):
        s = bytearray()
        s.append(146)
        s.append(colour1)
        s.append(colour2)
        if not self.g is None:
            self.g.write(s)

    '''
        Check for a kbd hit
    '''
    def kbhit(self):
        if self.f is None:
            return False
        rfd, wfd, xfd = select.select([self.f], [], [], 0.001)
        if len(rfd) == 0:
            return False
        return True

    '''
        Read character from kbd
    '''
    def readch(self):
        return self.f.read(1)

    '''
        Some often used colors
    '''
    def GREEN3(self):
        return 12

    def GREEN2(self):
        return 8

    def GREEN1(self):
        return 4

    def YELLOW3(self):
        return self.GREEN3() + self.RED3()

    def YELLOW2(self):
        return self.GREEN2() + self.RED2()

    def YELLOW1(self):
        return self.GREEN1() + self.RED1()

    def ORANGE1(self):
        return self.GREEN1() + self.RED2()

    def ORANGE2(self):
        return self.GREEN1() + self.RED3()

    def ORANGE3(self):
        return self.GREEN2() + self.RED3()

    def BROWN1(self):
        return self.GREEN2() + self.RED1()

    def BROWN2(self):
        return self.GREEN3() + self.RED1()

    def BROWN3(self):
        return self.GREEN3() + self.RED2()

    def RED3(self):
        return 3

    def RED2(self):
        return 2

    def RED1(self):
        return 1

    def BLACK(self):
        return 0

    '''
        Set color to red/green components
    '''
    def color2rg(self, color):
        red = color & 3
        green = color >> 2
        return red, green

    '''
        Change a led color
    '''
    def leds(self, row, col, color, flash = False):
        s = bytearray()
        if row < 0:
            s.append(176)
            s.append(16*6 + col + 8)
        else:
            s.append(144)
            s.append(16*row + col)
        red, green = self.color2rg(color)
        if flash:
            clearbg = 8
            copytobg = 0
        else:
            clearbg = 0
            copytobg = 4
        s.append(red + (16 * green) + clearbg + copytobg)
        if not self.g is None:
            self.g.write(s)

    '''
        Decode button coordinates
    '''
    def GetButtonPressed(self):
        if self.kbhit():
            a = self.readch()
            if a == b'\x90' or a == b'\xb0':
                ch = self.readch()
                pos = int.from_bytes(ch, byteorder = 'little')
                row = pos >> 4
                col = pos & 15
                if a == b'\xb0':
                    row = -1
                    col = col - 8
                up = (self.readch() == b'\x00')
                return row, col, up
            if a == b'\x80':
                ''' Note off '''
                key = self.readch()
                velocity = self.readch()
        return None, None, None

if __name__ == '__main__':
    p = lpmini()
    p.reset()
    p.ctrlbuf(True, False,1,0)
    p.leds(0, 0, p.RED1())
    p.leds(1, 0, p.RED2())
    p.leds(2, 0, p.RED3())
    p.leds(0, 1, p.ORANGE1())
    p.leds(1, 1, p.ORANGE2())
    p.leds(2, 1, p.ORANGE3())
    p.leds(0, 2, p.YELLOW1())
    p.leds(1, 2, p.YELLOW2())
    p.leds(2, 2, p.YELLOW3())
    p.leds(0, 3, p.BROWN1())
    p.leds(1, 3, p.BROWN2())
    p.leds(2, 3, p.BROWN3())
    p.leds(0, 4, p.GREEN1())
    p.leds(1, 4, p.GREEN2())
    p.leds(2, 4, p.GREEN3(), True)
    p.ctrlbuf(False, True,1,0)
    while True:
        row, col, up = p.GetButtonPressed()
        if not row is None:
            if not up:
                p.leds(row, col, row & 3, True)
            p.leds(2, 4, p.GREEN3(), False)

