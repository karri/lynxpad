import sys
import json

dir=str(sys.argv[len(sys.argv)-1])
fname= dir + '.mus'
with open(fname, 'rb') as f:
   raw = f.read()
data = raw

def printmusic(f, data, w, s):
    f.write('const unsigned char ' + s + '[] = {')
    i = -1
    for d in data:
        if i > -1: 
            f.write(',')
        else:
            i = 0
        if i == 0:
            f.write('\n')
        i = i + 1
        if i >= w:
            i = 0
        if d < 10:
            f.write(' '+str(d))
        else:
            f.write(str(d))
    f.write('};\n')
    f.write('\n')

fname= dir + '.c'
with open(fname, 'w') as f:
    printmusic(f, data, 8, dir)
