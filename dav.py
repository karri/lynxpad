import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.clock import Clock
import time
import select
from lpmini import lpmini
import json
import os
from sndlib import sndlib
from playbeat import playbeat

class DavBox(Widget):
    pass

class DavGrid(Widget):
    pass

class DavApp(App):
    def build(self):
        self.path = '.'
        self.lpmini = lpmini()
        self.lpmini.ctrlbuf(False, True,1,0)
        self.soundlib = sndlib()
        self.names = self.soundlib.sndnames().copy()
        self.now = time.time()
        self.blinktime = self.now
        self.beatsPerBar = 4
        self.prevbeat = 0
        self.beat = 0
        self.prevqbeat = 0
        self.prevqbeatrow = 0
        self.qbeat = 0
        self.settick(4)
        self.chanOn = [ True, True, True, True ]
        self.copyfrom = None
        self.copysnd = None
        self.chan1 = [None, None, None, None, None, None, None, None]
        self.chan2 = [None, None, None, None, None, None, None, None]
        self.chan3 = [None, None, None, None, None, None, None, None]
        self.chan4 = [None, None, None, None, None, None, None, None]
        self.chan1playing = None
        self.chan2playing = None
        self.chan3playing = None
        self.chan4playing = None
        self.cliplibset = 0
        self.cliplib = []
        self.cliplibedit = None
        self.sndlibset = 0
        self.sndlib = []
        self.pressed = []
        self.pianosnd = None
        self.pianoC = 0
        self.rhythmmode = False
        self.tap = None
        self.plus8 = False
        self.plus16 = False
        self.showfrombeat = 0
        self.patternlen = 7
        self.eraseclip()
        self.playbeat = playbeat()
        Clock.schedule_once(self.startup)
        return DavGrid()

    '''
        Helper function to turn Kivy grid button index to same as launchpad
    '''
    def lednr(self, row, col):
        if row == -1:
           return 80 - col
        return 71 -(9 * row + col)

    '''
        Currently playing
    '''
    def ledsACTIVE(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.GREEN3())
        self.pad[self.lednr(row, col)].background_color = [0.4,0.8,0.4,0.85]

    '''
        Clips you can play
    '''
    def ledsCLIP(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.YELLOW3())
        self.pad[self.lednr(row, col)].background_color = [0.8,0.8,0.2,0.85]

    '''
        Clips in the library
    '''
    def ledsLIBCLIP(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.YELLOW1())
        self.pad[self.lednr(row, col)].background_color = [0.6,0.6,0.2,0.85]

    '''
        Sounds in the clip
    '''
    def ledsSOUND(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.RED3())
        self.pad[self.lednr(row, col)].background_color = [0.8,0.2,0.2,0.85]

    '''
        Sounds in the library
    '''
    def ledsLIBSOUND(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.RED1())
        self.pad[self.lednr(row, col)].background_color = [0.6,0.2,0.2,0.85]

    '''
        Nothing
    '''
    def ledsEMPTY(self, row, col):
        self.lpmini.leds(row,col,self.lpmini.BLACK())
        self.pad[self.lednr(row, col)].background_color = [0.4,0.4,0.4,0.85]

    '''
        Set the minimum number of interrupts 1..N
        This affects the metronome
    '''
    def settick(self, ticks):
        self.tick = ticks
        if self.tick < 1:
            self.tick = 1
        interrupts = 60
        self.sixteenthtime = self.tick * 3 / interrupts
        self.trioletime = self.tick * 4 / interrupts

    '''
        Setbeat in seconds
        This is intended to be used by tapping in the desired tempo
    '''
    def setbeat(self, quartertime):
        self.settick(int((quartertime * 60 + 6) / 12))
        self.operation.text = 'Tempo is ' + str(int(15/self.sixteenthtime))

    '''
        Play the beat
    '''
    def playbeats(self):
        if self.rhythmmode:
            if len(self.currclip[self.beat][self.qbeat]) > 1:
                sound = self.currclip[self.beat][self.qbeat]
                if sound[1] < 8:
                    offset = 0
                else:
                    offset = self.currcliptone
                self.playbeat.play(sound[1], sound[0] + offset)

    '''
        Show a moving beat LED
    '''
    def showbeat(self):
        self.ledsEMPTY(self.prevbeat % 8, 8)
        self.ledsLIBCLIP(self.beat % 8, 8)
        self.prevbeat = self.beat
        if self.rhythmmode:
            if self.prevqbeatrow >= self.showfrombeat and self.prevqbeatrow < self.showfrombeat + 8:
                if len(self.currclip[self.prevqbeatrow][self.prevqbeat]) == 0:
                    self.ledsEMPTY(self.prevqbeatrow % 8,self.prevqbeat)
                else:
                    if len(self.currclip[self.prevqbeatrow][self.prevqbeat]) == 1:
                        self.ledsLIBSOUND(self.prevqbeatrow % 8,self.prevqbeat)
                    else:
                        self.ledsSOUND(self.prevqbeatrow % 8,self.prevqbeat)
            self.prevqbeatrow = self.beat
            self.prevqbeat = self.qbeat
            if self.beat >= self.showfrombeat and self.beat < self.showfrombeat + 8:
                self.ledsCLIP(self.beat % 8,self.qbeat)
        if self.qbeat == 0:
            self.ledsCLIP(-1,6)
        else:
            self.ledsEMPTY(-1,6)

    '''
        Check timing for the beat
        Note: the pattern follows currentclip. For performance mode I have to think about
        how to choose the self.currcliplen. It could be the longest clip playing.
    '''
    def nextbeat(self):
        elementtime = self.sixteenthtime
        elementnr = 4
        if self.currclippattern == 3:
            elementtime = self.trioletime
            elementnr = 3
        if time.time() - self.now > elementtime:
            self.now = self.now + elementtime
            self.playbeats()
            self.showbeat()
            self.qbeat = self.qbeat + 1
            if self.qbeat >= elementnr:
                self.qbeat = 0
                self.beat = self.beat + 1
                if self.beat > self.currcliplen:
                    self.beat = 0

    '''
        Scroll piano up/down
    '''
    def changePianoC(self, incr):
        self.pianoC = self.pianoC + incr
        self.operation.text = 'Scroll piano kbd ' + str(self.pianoC)

    '''
        Scroll piano help routine
    '''
    def pianoywrap(self, row):
        row = row + self.pianoC
        while row < 0:
            row = row + 7
        while row > 6:
            row = row - 7
        return row

    '''
        Draw piano
    '''
    def drawpiano(self):
        for y in range(0,8):
            self.ledsCLIP(y, 7)
        self.ledsLIBCLIP(self.pianoywrap(0), 6)
        if self.pianosnd is None:
            self.ledsLIBSOUND(self.pianoywrap(4), 6)
        elif self.pianosnd[0]:
            self.ledsLIBSOUND(self.pianoywrap(4), 6)
        else:
            self.ledsLIBCLIP(self.pianoywrap(4), 6)
        self.ledsLIBCLIP(self.pianoywrap(7), 6)
        self.ledsEMPTY(self.pianoywrap(1), 6)
        self.ledsEMPTY(self.pianoywrap(2), 6)
        self.ledsEMPTY(self.pianoywrap(3), 6)
        self.ledsEMPTY(self.pianoywrap(5), 6)
        self.ledsEMPTY(self.pianoywrap(6), 6)
        self.ledsEMPTY(self.pianoywrap(8), 6)
        self.ledsSOUND(0, 6)
        self.ledsSOUND(7, 6)

    '''
        Refresh
    '''
    def refresh(self, col = None):
        if col is None or col < 4:
            self.drawchans()
        if col is None or col > 5:
            self.drawpiano()
        if col is None or col == 5:
            self.drawsndlib()
        if col is None or col == 4:
            self.drawcliplib()

    '''
        Draw the top buttons 1,2,3,4
    '''
    def drawchanctrls(self):
        for chan in range(0,4):
            if self.rhythmmode:
                if chan == 0:
                    if self.plus8:
                        self.ledsCLIP(-1, chan)
                    else:
                        self.ledsEMPTY(-1, chan)
                elif chan == 1:
                    if self.plus16:
                        self.ledsCLIP(-1, chan)
                    else:
                        self.ledsEMPTY(-1, chan)
                else:
                    if chan + 1 == self.currclippattern:
                        self.ledsCLIP(-1, chan)
                    else:
                        self.ledsEMPTY(-1, chan)
            else:
                if self.chanOn[chan]:
                    self.ledsACTIVE(-1, chan)
                else:
                    self.ledsEMPTY(-1, chan)

    '''
       Mute control of channels
    '''
    def togglemutectrl(self, chan):
        self.chanOn[chan] = not self.chanOn[chan]
        if self.chanOn[chan]:
            self.operation.text = 'Unmute column ' + str(chan+1)
        else:
            self.operation.text = 'Mute column ' + str(chan+1)
        self.drawchanctrls()

    '''
       Set note lengths
       This works in rhythmmode only
    '''
    def notelen(self, val):
        if val == 2:
            self.operation.text = 'Set triole mode'
            self.currclippattern = 3
            self.saveclip()
        elif val == 3:
            self.operation.text = 'Set sixteenth note mode'
            self.currclippattern = 4
            self.saveclip()
        else:
            self.showfrombeat = 0
            if val == 0:
                self.plus8 = not self.plus8
            elif val == 1:
                self.plus16 = not self.plus16
            if self.plus8:
                self.showfrombeat = 8
            if self.plus16:
                self.showfrombeat = self.showfrombeat + 16
            self.operation.text = 'Show pattern from beat ' + str(self.showfrombeat)
        self.drawfrombeat()
        self.drawchanctrls()

    '''
        Draw the current rhythmmode
    '''
    def drawrhythmmode(self):
        if self.rhythmmode:
            self.operation.text = 'Build a clip mode'
            self.ledsSOUND(-1, 7)
        else:
            self.operation.text = 'Performance mode'
            self.ledsACTIVE(-1, 7)
        '''
            Perhaps I add a library mode for choosing/saving clips to a collection, like Hip-Hop, Bossanova...
        else:
            self.operation.text = 'Music collection mode'
            self.ledsCLIP(-1, 7)
        '''

    '''
        Draw Cliblib column
    '''
    def drawcliplib(self):
        for y in range(0,8):
            if y >= len(self.cliplib[self.cliplibset]):
                self.ledsEMPTY(y, 4)
            else:
                if self.cliplib[self.cliplibset][y] is None:
                    self.ledsEMPTY(y, 4)
                else:
                    if not self.rhythmmode:
                        self.ledsLIBCLIP(y, 4)
                    else:
                        if self.cliplibedit is None:
                            self.ledsLIBCLIP(y, 4)
                        else:
                            if self.cliplibedit[0] != self.cliplibset:
                                self.ledsLIBCLIP(y, 4)
                            else:
                                if self.cliplibedit[1] != y:
                                    self.ledsLIBCLIP(y, 4)
                                else:
                                    self.ledsACTIVE(y, 4)

    '''
        Draw the currclip starting at self.showfrombeat
    '''
    def drawfrombeat(self):
        startrow = self.showfrombeat
        for y in range(startrow, startrow + 8):
            for x in range(0,4):
                if len(self.currclip[y][x]) == 0:
                    self.ledsEMPTY(y % 8, x)
                else:
                    if len(self.currclip[y][x]) == 1:
                        self.ledsLIBSOUND(y % 8, x)
                    else:
                        self.ledsSOUND(y % 8, x)

    '''
        Draw Sndlib column
    '''
    def drawsndlib(self):
        for y in range(0,8):
            if y >= len(self.sndlib[self.sndlibset]):
                self.ledsEMPTY(y, 5)
            else:
                if self.sndlib[self.sndlibset][y] == None:
                    self.ledsEMPTY(y, 5)
                else:
                    self.ledsLIBSOUND(y, 5)

    '''
        Map tone to name
    '''
    def maptone(self, tone):
        if tone % 12 == 11:
            return 'B'
        if tone % 12 == 10:
            return 'A#'
        if tone % 12 == 9:
            return 'A'
        if tone % 12 == 8:
            return 'G#'
        if tone % 12 == 7:
            return 'G'
        if tone % 12 == 6:
            return 'F#'
        if tone % 12 == 5:
            return 'F'
        if tone % 12 == 4:
            return 'E'
        if tone % 12 == 3:
            return 'D#'
        if tone % 12 == 2:
            return 'D'
        if tone % 12 == 1:
            return 'C#'
        if tone % 12 == 0:
            return 'C'
        return str(tone)

    '''
        Mark note on/off square
    '''
    def isDuration(self):
        if self.copyfrom[1] < 4:
            return True
        return False

    '''
        Insert snd to self.currclip
    '''
    def insertsnd(self, row, col):
        if self.copysnd is None:
            ''' Erase '''
            if self.isDuration():
                self.currclip[row + self.showfrombeat][col] = [0]
                self.operation.text = 'Add duration'
                self.ledsLIBSOUND(row, col)
                self.saveclip()
            else:
                self.currclip[row + self.showfrombeat][col] = []
                self.operation.text = 'Erase sound'
                self.ledsEMPTY(row, col)
                self.saveclip()
        else:
            if self.copysnd[0]:
                if self.isDuration():
                    self.currclip[row + self.showfrombeat][col] = [0]
                    self.operation.text = 'Add duration'
                    self.ledsLIBSOUND(row, col)
                    self.saveclip()
                else: 
                    self.currclip[row + self.showfrombeat][col] = [self.copysnd[1],
                        self.copysnd[2]]
                    self.operation.text = self.maptone(self.copysnd[1]) + ' ' + str(self.copysnd[2])
                    self.ledsSOUND(row, col)
                    self.saveclip()
        
    '''
        Insert clip
    '''
    def insertclip(self, row, col):
        if self.copysnd is None:
            ''' Erase '''
            if col == 0:
                self.chan1[row] = None
            elif col == 1:
                self.chan2[row] = None
            elif col == 2:
                self.chan3[row] = None
            elif col == 3:
                self.chan4[row] = None
            self.operation.text = 'Erase clip from ' + str(row) + str(col)
            self.savechans()
            return False
        else:
            if not self.copysnd[0]:
                if col == 0:
                    self.chan1[row] = self.copysnd.copy()
                elif col == 1:
                    self.chan2[row] = self.copysnd.copy()
                elif col == 2:
                    self.chan3[row] = self.copysnd.copy()
                elif col == 3:
                    self.chan4[row] = self.copysnd.copy()
                self.savechans()
                self.operation.text = 'Insert clip to ' + str(row) + str(col)
                return True

    '''
        Draw chan 1
    '''
    def drawchan1(self):
        for y in range(0,8):
            if self.chan1[y] is None:
                self.ledsEMPTY(y, 0)
            else:
                if self.chan1playing == y:
                    self.ledsACTIVE(y, 0)
                else:
                    self.ledsCLIP(y, 0)

    '''
        Draw chan 2
    '''
    def drawchan2(self):
        for y in range(0,8):
            if self.chan2[y] is None:
                self.ledsEMPTY(y, 1)
            else:
                if self.chan2playing == y:
                    self.ledsACTIVE(y, 1)
                else:
                    self.ledsCLIP(y, 1)

    '''
        Draw chan 3
    '''
    def drawchan3(self):
        for y in range(0,8):
            if self.chan3[y] is None:
                self.ledsEMPTY(y, 2)
            else:
                if self.chan3playing == y:
                    self.ledsACTIVE(y, 2)
                else:
                    self.ledsCLIP(y, 2)

    '''
        Draw chan 4
    '''
    def drawchan4(self):
        for y in range(0,8):
            if self.chan4[y] is None:
                self.ledsEMPTY(y, 3)
            else:
                if self.chan4playing == y:
                    self.ledsACTIVE(y, 3)
                else:
                    self.ledsCLIP(y, 3)

    '''
        Clear playfield
    '''
    def drawchans(self):
        self.drawchan1()
        self.drawchan2()
        self.drawchan3()
        self.drawchan4()

    '''
        isEmpty
    '''
    def isEmpty(self, val):
        if len(val) == 0:
            return True
        for item in val:
            if item != 0:
                return False
        return True

    '''
        Set color
    '''
    def ledscolor(self, row, col, val):
        if val == 1:
            self.ledsLIBSOUND(row, col)
            return
        if val == 2:
            self.ledsSOUND(row, col)
            return
        if val == 3:
            self.ledsLIBCLIP(row, col)
            return
        if val == 4:
            self.ledsCLIP(row, col)
            return
        if val == 5:
            self.ledsACTIVE(row, col)
            return
        self.ledsEMPTY(row, col)

    '''
        Choose next cliplibset
    '''
    def nextcliplibset(self):
        self.cliplibset = (self.cliplibset + 1) % 6
        self.ledscolor(-1, 4, self.cliplibset)
        self.operation.text = 'Clip set ' + str(self.cliplibset + 1)

    '''
        Choose next sndlibset
    '''
    def nextsndlibset(self):
        self.sndlibset = (self.sndlibset + 1) % 6
        self.ledscolor(-1, 5, self.sndlibset)
        self.operation.text = 'Sound set ' + str(self.sndlibset + 1)

    '''
        Set clip length
    '''
    def setcurrcliplen(self, val):
        self.currcliplen = val
        self.saveclip()

    '''
        Pad button callback
    '''
    def btn(self, row, col, up):
        self.processui(row, col, up)

    '''
        Copy the clip/snd from cell
    '''
    def getcopydata(self, row, col):
        '''
            Copy clips from clip library
        '''
        if col == 4:
            if self.cliplib[self.cliplibset][row] is None:
                return None
            else:
                return [False, 0, row + self.cliplibset * 8]
        '''
            Copy snds from snd library
        '''
        if col == 5:
            if self.sndlib[self.sndlibset][row] is None:
                return None
            else:
                return [True, 0, row + self.sndlibset * 8]
        '''
            Copy snds from piano kbg ( there has to be a snd in pianosnd )
        '''
        if not self.pianosnd is None:
            if col == 6:
                ''' Copy from sharp notes '''
                note = self.pianoC + 7 - row
                octave = 0
                if note % 7 == 1:
                    ''' C# '''
                    return [self.pianosnd[0], octave + 1, self.pianosnd[2]]
                elif note % 7 == 2:
                    ''' D# '''
                    return [self.pianosnd[0], octave + 3, self.pianosnd[2]]
                elif note % 7 == 4:
                    ''' F# '''
                    return [self.pianosnd[0], octave + 6, self.pianosnd[2]]
                elif note % 7 == 5:
                    ''' G# '''
                    return [self.pianosnd[0], octave + 8, self.pianosnd[2]]
                elif note % 7 == 6:
                    ''' A# '''
                    return [self.pianosnd[0], octave + 10, self.pianosnd[2]]
            elif col == 7:
                ''' Copy basic notes '''
                note = self.pianoC + 7 - row
                octave = 0
                if note % 7 == 0:
                    ''' C '''
                    return [self.pianosnd[0], octave + 0, self.pianosnd[2]]
                if note % 7 == 1:
                    ''' D '''
                    return [self.pianosnd[0], octave + 2, self.pianosnd[2]]
                if note % 7 == 2:
                    ''' E '''
                    return [self.pianosnd[0], octave + 4, self.pianosnd[2]]
                if note % 7 == 3:
                    ''' F '''
                    return [self.pianosnd[0], octave + 5, self.pianosnd[2]]
                if note % 7 == 4:
                    ''' G '''
                    return [self.pianosnd[0], octave + 7, self.pianosnd[2]]
                if note % 7 == 5:
                    ''' A '''
                    return [self.pianosnd[0], octave + 9, self.pianosnd[2]]
                if note % 7 == 6:
                    ''' B '''
                    return [self.pianosnd[0], octave + 11, self.pianosnd[2]]
        return None

    '''
        Is copy from this location allowed
    '''
    def copyallowed(self, row, col):
        if row == -1 or col == 8:
            return False
        if col == 7:
            return True
        if col == 6:
            note = self.pianoC + 7 - row
            if row == 0 or row == 7 or note % 7 == 3:
                return False
            else:
                return True
        if col == 5:
            return True
        if self.rhythmmode:
            if col == 4:
                '''
                    Create new clip from scratch
                '''
                if self.cliplib[self.cliplibset][row] is None:
                    self.cliplibedit = [self.cliplibset, row]
                    self.eraseclip()
                    self.saveclip()
                    self.pressCliplibColumnButton(row, col, False)
                    self.readclips()
                    self.drawcliplib()
                return False
            if col < 4:
                return True
            return False
        else:
            if col == 4:
                return True
            return False

    '''
        Is paste to this location allowed
    '''
    def pasteallowed(self, row, col):
        if row == -1 or col == 4 or col == 5 or col == 7 or col == 8:
            return False
        if col == 6:
            if self.copysnd is None:
                return False
            note = self.pianoC + 6 - row
            if note % 7 == 2:
                return True
            else:
                return False
        if self.rhythmmode:
            if col < 4 and self.cliplibedit == None:
                self.operation.text = 'Create a new clip by long press in column 5'
                return False
            if self.copysnd is None:
                return True
            if self.copysnd[0]:
                return True
        else:
            if self.copysnd is None:
                return True
            if not self.copysnd[0]:
                return True
        return False

    '''
        Handle release of button
    '''
    def releasebutton(self, row, col, up):
        newpressed = []
        for item in self.pressed:
            if row == item[0] and col == item[1]:
                 dur = time.time() - item[2]
                 if dur > 0.3 and self.copyallowed(row, col):
                     self.copyfrom = [row, col]
                     self.copysnd = self.getcopydata(row, col)
                     if self.copysnd is None:
                         self.operation.text = 'Delete'
                     elif self.copysnd[0]:
                         self.operation.text = 'Copy sound ' + str(self.copysnd[1]) + str(self.copysnd[2])
                     else:
                         self.operation.text = 'Copy clip ' + str(self.copysnd[2]) + ' at ' + self.maptone(self.copysnd[1])
            else:
                newpressed.append(item)
        self.pressed = newpressed

    '''
        Handle press of top row buttons
    '''
    def pressTopRowButton(self, row, col, up):
        if col < 4:
            if self.rhythmmode:
                ''' Build rhythm buttons '''
                self.drawchans()
                self.notelen(col)
            else:
                ''' Channels mute/play '''
                self.togglemutectrl(col)
        if col == 4:
            ''' Switch through clip library '''
            self.nextcliplibset()
            self.drawcliplib()
        if col == 5:
            ''' Switch through snd library '''
            self.nextsndlibset()
            self.drawsndlib()
        if col == 6:
            ''' Tap a new tempo '''
            now = time.time()
            if self.tap is None:
                self.operation.text = 'Tap tempo'
                self.tap = now
            elif now - self.tap > 2:
                self.tap = now
            else:
                tempo = now - self.tap
                self.tap = now
                self.setbeat(tempo)
        if col == 7:
            ''' Switch between rhythmmode and normal '''
            self.rhythmmode = not self.rhythmmode
            self.drawrhythmmode()
            self.drawchans()
            if self.rhythmmode:
                self.drawfrombeat()
            else:
                self.cliplibedit = None
                self.eraseclip()
                self.drawcliplib()
            self.drawchanctrls()

    '''
        Handle press of rightmost column buttons
    '''
    def pressRightmostColumnButton(self, row, col, up):
        ''' Handle A, B, C... column '''
        if self.rhythmmode:
            ''' Adjust length of current clip '''
            val = row
            if self.plus8:
                val = val + 8
            if self.plus16:
                val = val + 16
            self.setcurrcliplen(val)
            self.operation.text = 'Set clip length to ' + str(val+1)
        else:
            ''' Activate whole row 1..4 '''
            if self.chan1[row] is None:
                self.chan1playing = None
            else:
                self.chan1playing = row
            if self.chan2[row] is None:
                self.chan2playing = None
            else:
                self.chan2playing = row
            if self.chan3[row] is None:
                self.chan3playing = None
            else:
                self.chan3playing = row
            if self.chan4[row] is None:
                self.chan4playing = None
            else:
                self.chan4playing = row
            self.drawchans()
            self.operation.text = 'Play row ' + str(row + 1)

    '''
        Handle piano sharp keys column
    '''
    def pressPianoSharpColumnButton(self, row, col, up):
        if row == 0:
            ''' Scroll piano up '''
            self.changePianoC(1)
            self.drawpiano()
        elif row == 7:
            ''' Scroll piano down '''
            self.changePianoC(-1)
            self.drawpiano()
        else:
            ''' Play sharp notes '''
            note = self.pianoC + 7 - row
            if note % 7 == 1:
                self.operation.text = 'C# 1'
            elif note % 7 == 2:
                self.operation.text = 'D# 3'
            elif note % 7 == 4:
                self.operation.text = 'F# 6'
            elif note % 7 == 5:
                self.operation.text = 'G# 8'
            elif note % 7 == 6:
                self.operation.text = 'A# 10'
            elif note % 7 == 3:
                if self.pasteallowed(row, col):
                    ''' Paste sound '''
                    ''' Stop blinking source '''
                    self.pianosnd = self.copysnd
                    if self.copysnd is None:
                        self.refresh(self.copyfrom[1])
                    else:
                        if self.copysnd[0]:
                            self.refresh(self.copyfrom[1])
                            self.operation.text = 'New piano sound ' + str(self.pianosnd[2])
                        else:
                            self.refresh(self.copyfrom[1])
                            self.operation.text = 'New piano clip ' + str(self.pianosnd[2])
                    self.copyfrom = None
                    self.copysnd = None
                    self.drawpiano()
            else:
                pass

    '''
        Handle piano neutral keys column
    '''
    def pressPianoColumnButton(self, row, col, up):
        note = self.pianoC + 7 - row
        if note % 7 == 0:
            self.operation.text = 'C 0'
        if note % 7 == 1:
            self.operation.text = 'D 2'
        if note % 7 == 2:
            self.operation.text = 'E 4'
        if note % 7 == 3:
            self.operation.text = 'F 5'
        if note % 7 == 4:
            self.operation.text = 'G 7'
        if note % 7 == 5:
            self.operation.text = 'A 9'
        if note % 7 == 6:
            self.operation.text = 'B 11'

    '''
        Handle Cliplib column
        Choose clip to edit
    '''
    def pressCliplibColumnButton(self, row, col, up):
        if self.rhythmmode:
            nr = self.cliplibset * 8 + row
            self.cliplibedit = [self.cliplibset, row]
            self.operation.text = 'Edit clip ' + str(nr)
            self.loadclip(nr)
            self.drawcliplib()
            self.drawfrombeat()
        else:
            if self.copyfrom is None or self.pasteallowed(row, col) == False:
                ''' Play libclip '''
                nr = self.cliplibset * 8 + row
                self.operation.text = 'Play clip ' + str(nr)

    '''
        Handle Sndlib column
    '''
    def pressSndlibColumnButton(self, row, col, up):
        if self.rhythmmode:
            if self.copyfrom is None or self.pasteallowed(row, col) == False:
                ''' Play libsound '''
                nr = self.sndlibset * 8 + row
                self.operation.text = 'Play sound ' + str(nr)
            else:
                ''' Paste sound '''
                ''' Stop blinking source '''
                if self.copysnd is None:
                    self.refresh(self.copyfrom[1])
                else:
                    self.refresh(self.copyfrom[1])
                ''' Insert it into currentclip '''
                self.insertsnd(row, col)
                self.copyfrom = None
                self.copysnd = None
        else:
            pass

    '''
        Handle press of button
    '''
    def pressbutton(self, row, col, up):
        self.pressed.append([row, col, time.time()])
        if row == -1:
            self.pressTopRowButton(row, col, up)
        elif col == 4:
            self.pressCliplibColumnButton(row, col, up)
        elif col == 5:
            self.pressSndlibColumnButton(row, col, up)
        elif col == 6:
            self.pressPianoSharpColumnButton(row, col, up)
        elif col == 7:
            self.pressPianoColumnButton(row, col, up)
        elif col == 8:
            self.pressRightmostColumnButton(row, col, up)
        else:
            ''' Interact with clips and sounds in play area '''
            if self.rhythmmode:
                if self.copyfrom is None or self.pasteallowed(row, col) == False:
                    pass
                else:
                    ''' Paste sound '''
                    ''' Stop blinking source '''
                    self.drawpiano()
                    self.drawsndlib()
                    ''' Insert it into currentclip '''
                    self.insertsnd(row, col)
                    self.copyfrom = None
                    self.copysnd = None
            else:
                if self.copyfrom is None or self.pasteallowed(row, col) == False:
                    ''' Play clips '''
                    if col == 0:
                        if self.chan1[row] is None:
                            self.chan1playing = None
                        else:
                            self.chan1playing = row
                        self.drawchan1()
                    if col == 1:
                        if self.chan2[row] is None:
                            self.chan2playing = None
                        else:
                            self.chan2playing = row
                        self.drawchan2()
                    if col == 2:
                        if self.chan3[row] is None:
                            self.chan3playing = None
                        else:
                            self.chan3playing = row
                        self.drawchan3()
                    if col == 3:
                        if self.chan4[row] is None:
                            self.chan4playing = None
                        else:
                            self.chan4playing = row
                        self.drawchan4()
                else:
                    ''' Paste sound '''
                    ''' Stop blinking source '''
                    if self.copysnd is None:
                        self.refresh(self.copyfrom[1])
                    else:
                        self.refresh(self.copyfrom[1])
                        ''' Insert it into current channel '''
                    self.insertclip(row, col)
                    self.drawchans()
                    self.copyfrom = None
                    self.copysnd = None

    '''
        Read character from kbd
        For copy/paste we may need to keep track on long presses
    '''
    def processui(self, row, col, up):
        if not row is None:
            if up:
                self.releasebutton(row, col, up)
            else:
                self.pressbutton(row, col, up)

    def readclips(self):
        files = []
        for f in os.listdir(self.path):
            if 'clip' in f and 'json' in f:
                f = int(f.strip('clip.json'),10)
                files.append(f)
        self.cliplib = []
        for j in range(0, 6):
            section = []
            for i in range(0,8):
                nr = 8 * j + i
                if nr in files:
                    section.append(nr)
                else:
                    section.append(None)
            self.cliplib.append(section.copy())

    def readsnds(self):
        self.sndlib = []
        for j in range(0, 6):
            section = []
            for i in range(0,8):
                nr = 8 * j + i
                print(nr, len(self.names))
                if self.names[nr] is None:
                    section.append(None)
                else:
                    section.append(nr)
            self.sndlib.append(section.copy())

    def startup(self, dt):
        self.app = App.get_running_app()
        self.box = self.app.root.children[0].children
        self.operation = self.box[0]
        self.pad = self.box[1].children
        self.drawrhythmmode()
        self.drawchanctrls()
        self.drawpiano()
        self.readclips()
        self.readsnds()
        self.drawcliplib()
        self.drawsndlib()
        self.loadchans()
        self.drawchans()
        Clock.schedule_once(self.periodic, 1/60)

    def eraseclip(self):
        self.currcliplen = 7
        self.currclippattern = 4
        self.currcliptone = 0
        self.currclip = [[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]],
                         [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]]]

    def loadclip(self, nr):
        fname = self.path + '/clip%02d.json' % nr
        if os.path.exists(fname):
            with open(fname) as f:
                data = json.loads(f.read())
                self.currclippattern = data['p']
                self.currcliplen = data['l']
                self.currclip = data['d'].copy()
                self.currcliptone = data['t']
        else:
            self.eraseclip()

    def saveclip(self):
        if self.cliplibedit is None:
            return
        nr = self.cliplibedit[0] * 8 + self.cliplibedit[1]
        fname = self.path + '/clip%02d.json' % nr
        clip = {
            'p': self.currclippattern,
            'l': self.currcliplen,
            'd': self.currclip,
            't': self.currcliptone
        }
        with open(fname, 'w') as f:
            f.write(json.dumps(clip))

    def loadchans(self):
        fname = self.path + '/chans.json'
        if os.path.exists(fname):
            with open(fname) as f:
                data = json.loads(f.read())
                self.chan1 = data['ch1'].copy()
                self.chan2 = data['ch2'].copy()
                self.chan3 = data['ch3'].copy()
                self.chan4 = data['ch4'].copy()
        else:
            self.eraseclip()

    def savechans(self):
        fname = self.path + '/chans.json'
        chans = {
            'ch1': self.chan1,
            'ch2': self.chan2,
            'ch3': self.chan3,
            'ch4': self.chan4
        }
        with open(fname, 'w') as f:
            f.write(json.dumps(chans))

    def periodic(self, dt):
        if not self.copyfrom is None:
            now = time.time()
            if now > self.blinktime + 0.5:
                self.ledsEMPTY(self.copyfrom[0], self.copyfrom[1])
                if now > self.blinktime + 1:
                    self.blinktime = now
            elif now > self.blinktime:
                if self.copyfrom[1] < 4:
                    if self.rhythmmode:
                        self.ledsSOUND(self.copyfrom[0], self.copyfrom[1])
                    else:
                        self.ledsCLIP(self.copyfrom[0], self.copyfrom[1])
                elif self.copyfrom[1] == 4:
                    self.ledsLIBCLIP(self.copyfrom[0], self.copyfrom[1])
                elif self.copyfrom[1] == 5:
                    self.ledsLIBSOUND(self.copyfrom[0], self.copyfrom[1])
                else:
                    self.ledsCLIP(self.copyfrom[0], self.copyfrom[1])
        self.nextbeat()
        row, col, up = self.lpmini.GetButtonPressed()
        self.processui(row, col, up)
        Clock.schedule_once(self.periodic, 1/60)

if __name__ == "__main__":
    DavApp().run()
